API documentation can be found [HERE](https://talikert.gitlab.io/chessmove-server/apidoc/)

# QUICK SETUP

_Launching the app_:
1. Download [artifact](https://gitlab.com/TaliKert/chessmove-server/-/jobs/370909752/artifacts/download) and extract
2. Double click JAR (*Open with Java 11*)
3. Open "localhost:24377" in browser

_To simulate a chess board_:
1. Download [source](https://gitlab.com/TaliKert/chessmove-server/-/archive/master/chessmove-server-master.zip) and extract /testboard folder
2. Open CMD in testboard folder and run `python testboard.py localhost:24377 long_game_black_surrender.txt gameIllegal.txt` (need to have python on path)

To see game centipawn score, see section **Chess engine functionality** below.


# FULL INSTRUCTIONS

## ChessMove

_Prerequisites_:
* Java 11

### Deploying the executable (.jar)

To find the latest executable, go to [pipelines](https://gitlab.com/TaliKert/chessmove-server/pipelines), seek out the latest passing build and download its _build artifacts_.

_Deployment instructions_:  
1. Run the executable on the command line with `java -jar $PATH_TO_EXECUTABLE`  
      ***OR***  
Double click the `.jar` file (bear in mind, that there will be no indication of the server running when running it this way, other than the process list)  
2. Open "localhost:24377" in a web browser



### Compiling from source

_Compiling instructions_:
1. From the project root, run `./gradlew bootJar`
2. The executable will be in `$PROJECT_ROOT/build/libs/`

### Board mocking  
As the functionality of this application is limited without any data, we have provided a python script to easily mock chessboard(s) using `.txt` files, where the moves are represented in a simple  **from -> to**  coordinate notation.  
The script requires ***Python 3*** and  ***cURL***.

_Test script instructions_:
1. Download the contents of the "testboard" directory from the root of the repository
2. With the server running, run the script on command line with `python testboard.py localhost:24377 gameKAI.txt` 
3. To simulate two boards at once run `python testboard.py localhost:24377 gameKAI.txt gameKAI.txt` 

If you wish to mock the board manually, please refer to the board API documentation:
[talikert.gitlab.io/chessmove-server/apidoc/](https://talikert.gitlab.io/chessmove-server/apidoc/)

### Chess engine functionality
To get game position evaluation working, a UCI compatible chess engine is required.
We highly recommend to use **Stockfish**.

_instructions_:
1. Download the Stockfish chess engine for your system from [stockfishchess.org/download/](https://stockfishchess.org/download/)
2. Find and copy the executable (eg. Windows/stockfish_10_x64.exe) to the root of the servers classpath (ie. the same folder as your **.jar** and **chessmove.db** files)
3. Rename the file to "stockfish.exe"
4. *[Linux/Mac]* Check that the file permissions allow for execution by the Java process (ie. everyone should have read and execution rights to the engine).
5. The board view should now show which player has the advantage in a given position.