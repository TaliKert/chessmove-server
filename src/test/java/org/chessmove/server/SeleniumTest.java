package org.chessmove.server;

import com.google.gson.Gson;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.chessmove.server.model.dto.MoveDto;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SeleniumTest {

  WebDriver driver;

  WebDriverWait wait;

  String baseUriStringPrefix = "http://localhost:";

  /**
   * Assumed to be assigned by the SystemTest... classes.
   */
  String baseUriString;

  /**
   * Assumed to be assigned by the SystemTest... classes.
   */
  Gson gson;

  Logger logger = LoggerFactory.getLogger(this.getClass());

  @BeforeClass
  public static void setupClass() {
    WebDriverManager
        .chromedriver()
        .setup();
  }

  @Before
  public void setupTest() {
    driver = new ChromeDriver(
        new ChromeOptions()
            .addArguments("--headless", "--disable-gpu", "--no-sandbox", "--window-size=1920,1080")
    );
    wait = new WebDriverWait(driver, 10);
  }

  @After
  public void teardown() {
    if (driver != null) {
      driver.quit();
    }
  }

  List<MoveDto> assembleMoveDTOs(String[] froms, String[] tos) {
    List<MoveDto> moves = new ArrayList<>();

    for (int i = 0; i < froms.length; i++) {
      MoveDto moveDto = new MoveDto();

      moveDto.setFrom(froms[i]);
      moveDto.setTo(tos[i]);

      moves.add(moveDto);
    }

    return moves;
  }

  WebElement findElement(By by) {
    logger.info("Looking for an element with by=\"{}\"", by);
    return driver.findElement(by);
  }

  List<WebElement> findElements(By by) {
    logger.info("Looking for elements with by=\"{}\"", by);
    List<WebElement> elements = driver.findElements(by);

    logger.info("Found {} elements", elements.size());

    return elements;
  }

  void apiStartGame(int boardId) throws IOException, InterruptedException {
    logger.info("Sending a request to start a game with boardID={}", boardId);

    assertEquals(
        HttpStatus.SC_CREATED,
        HttpClientBuilder
            .create()
            .build()
            .execute(new HttpPost(baseUriString + "/api/start/" + boardId))
            .getStatusLine()
            .getStatusCode());

    Thread.sleep(120);
  }

  void apiMakeMove(int boardId, String from, String to) throws IOException, InterruptedException {
    MoveDto moveDto = new MoveDto();
    moveDto.setFrom(from);
    moveDto.setTo(to);

    apiMakeMove(boardId, moveDto);
  }

  void apiMakeMove(int boardId, MoveDto moveDto) throws IOException, InterruptedException {
    logger.info("Sending a move request on boardId={} with move=({}, {})",
        boardId, moveDto.from, moveDto.to);

    moveDto.setBoardId("" + boardId);

    HttpPost movePost = new HttpPost(baseUriString + "/api/move");
    movePost.setEntity(new StringEntity(gson.toJson(moveDto)));
    movePost.setHeader("Content-type", "application/json");

    int res = HttpClientBuilder
        .create()
        .build()
        .execute(movePost)
        .getStatusLine()
        .getStatusCode();

    assertEquals(
        HttpStatus.SC_CREATED,
        res);

    Thread.sleep(120);
  }

  void apiMakeMoves(int boardId, List<MoveDto> moves) throws IOException, InterruptedException {
    for (MoveDto move : moves)
      apiMakeMove(boardId, move);
  }

  void xhrEndGame(int boardId) throws IOException, InterruptedException {
    logger.info("Sending a request to end a game with boardID={}", boardId);

    assertEquals(
        HttpStatus.SC_OK,
        HttpClientBuilder
            .create()
            .build()
            .execute(new HttpPost(baseUriString + "/xhr/game/" + boardId + "/end/draw"))
            .getStatusLine()
            .getStatusCode());

    Thread.sleep(120);
  }

  void goToDashboard() throws InterruptedException {
    if (!baseUriString.equals(driver.getCurrentUrl())) {
      logger.info("Going to dashboard page");

      driver.get(baseUriString);

      Thread.sleep(120);
    }
  }

  void goToHistory() throws InterruptedException {
    if (!(baseUriString + "/history").equals(driver.getCurrentUrl())) {
      logger.info("Going to history page");

      driver.get(baseUriString + "/history");

      Thread.sleep(120);
    }
  }

  boolean checkDashboardContainsBoardId(int boardId) throws InterruptedException {
    logger.info("Checking if dashboard has a board with boardId={}", boardId);

    for (int visibleBoardId : dashboardGetVisibleBoardIds())
      if (visibleBoardId == boardId) return true;

    return false;
  }

  boolean checkDashboardBoardMoveCountEquals(int boardId, int expectedMoveCount) throws InterruptedException {
    logger.info("Checking if a board with boardId={} on dashboard has {} moves",
        boardId, expectedMoveCount);

    return expectedMoveCount == dashboardGetBoardMoveCount(boardId);
  }

  boolean checkDashboardCurrentGameIdIs(int gameId) {
    logger.info("Checking if the board with gameId={} view is open, currently at {}",
        gameId, driver.getCurrentUrl());

    return (baseUriString + "/game/" + gameId).equals(driver.getCurrentUrl());
  }

  boolean checkDashboardCurrentBoardMoveCountEquals(int expectedMoveCount) {
    logger.info("Checking if the current board view shows that the board {} moves",
        expectedMoveCount);

    return expectedMoveCount == dashboardCurrentBoardMoveCount();
  }

  List<Integer> dashboardGetVisibleBoardIds() throws InterruptedException {
    goToDashboard();

    List<WebElement> boardIdDivElements = findElements(By.xpath("//div[@data-boardid]"));

    List<Integer> output = new ArrayList<>(boardIdDivElements.size());

    for (WebElement element : boardIdDivElements)
      output.add(Integer.parseInt(element.getAttribute("data-boardid")));

    logger.info("Found boardIds({}): {}", output.size(), output);

    return output;
  }

  List<WebElement> dashboardGetVisibleBoardWebElements() throws InterruptedException {
    goToDashboard();

    return findElements(By.xpath("//a[.//div[@data-boardid]]"));
  }

  void dashboardClickBoard(int boardId) throws InterruptedException {
    goToDashboard();

    WebElement boardLinkElement = findElement(
        By.xpath(String.format("//a[.//div[@data-boardid='%d']]", boardId)));

    logger.info("Clicking on the board with boardId={}", boardId);
    boardLinkElement.click();

    Thread.sleep(120);
  }

  void dashboardCurrentBoardClickPreviousState() throws InterruptedException {
    WebElement previousStateButtonElement = findElement(
        By.xpath("//button[@id='move-back']"));

    logger.info("Clicking on the previous state button in the dashboard board view");
    previousStateButtonElement.click();

    Thread.sleep(120);
  }

  void dashboardCurrentBoardClickRevertState() throws InterruptedException {
    WebElement revertStateButtonElement = findElement(
        By.xpath("//button[@id='move-revert']"));

    logger.info("Clicking on the revert state button in the dashboard board view");
    revertStateButtonElement.click();

    Thread.sleep(120);
  }

  void dashboardCurrentBoardClickNextState() throws InterruptedException {
    WebElement nextStateButtonElement = findElement(
        By.xpath("//button[@id='move-forward']"));

    logger.info("Clicking on the next state button in the dashboard board view");
    nextStateButtonElement.click();

    Thread.sleep(120);
  }

  int dashboardGetBoardMoveCount(int boardId) throws InterruptedException {
    goToDashboard();

    WebElement moveCount = findElement(
        By.xpath(String.format("//span[@id='moves-%d']", boardId)));

    return Integer.parseInt(moveCount.getText().split(" ")[1]);
  }

  int dashboardFindBoardGameId(int boardId) throws InterruptedException {
    goToDashboard();

    dashboardClickBoard(boardId);

    WebElement gameIdElement = findElement(
        By.xpath("//span[text()='Game id:']/../span[2]"));

    return Integer.parseInt(gameIdElement.getText());
  }

  int dashboardCurrentBoardMoveCount() {
    logger.info("Finding the number of moves for the open board view in dashboard.");

    WebElement moveCountElement = findElement(
        By.xpath("//span[../span[text()='moves:' and @class='greyTxt'] and text()!='moves:']"));

    return Integer.parseInt(moveCountElement.getText());
  }

  int dashboardCurrentBoardCurrentStateMoveCount() {
    logger.info("Finding the number of moves for the open board view in dashboard.");

    WebElement moveCountElement = findElement(
        By.xpath("//span[../span[text()='move: ' and @class='greyTxt'] and not(@class) and text()!='move: ']"));

    return Integer.parseInt(moveCountElement.getText());
  }

  int dashboardCurrentBoardId() {
    logger.info("Finding the boardId of the currently open board view.");

    WebElement boardIdElement = findElement(
        By.xpath("//span[../span[text()='board id:' and @class='greyTxt'] and text()!='board id:']"));

    return Integer.parseInt(boardIdElement.getText());
  }

  void dashboardCurrentBoardSetWhiteName(String whiteName) throws InterruptedException {
    WebElement whiteNameFieldElement = findElement(
        By.xpath("//input[@name='white-name']"));

    logger.info("Entering {} in the name field of the white player", whiteName);
    whiteNameFieldElement.sendKeys(whiteName, Keys.ENTER);
  }

  void dashboardCurrentBoardSetBlackName(String blackName) {
    WebElement blackNameFieldElement = findElement(
        By.xpath("//input[@name='black-name']"));

    logger.info("Entering {} in the name field of the black player", blackName);
    blackNameFieldElement.sendKeys(blackName, Keys.ENTER);
  }

  String dashboardCurrentBoardGetWhiteName() {
    logger.info("Finding the name of the white player in the open board view in dashboard.");

    WebElement whiteNameFieldElement = findElement(
        By.xpath("//input[@name='white-name']"));

    return whiteNameFieldElement.getAttribute("value");
  }

  String dashboardCurrentBoardGetBlackName() {
    logger.info("Finding the name of the black player in the open board view in dashboard.");

    WebElement blackNameFieldElement = findElement(
        By.xpath("//input[@name='black-name']"));

    return blackNameFieldElement.getAttribute("value");
  }

  void dashboardGoToBoardView(int boardId) throws InterruptedException {
    goToDashboard();

    assertTrue(checkDashboardContainsBoardId(boardId));

    dashboardClickBoard(boardId);

    assertEquals(boardId, dashboardCurrentBoardId());
  }

  void dashboardCurrentBoardClickSubmitNamesButton() throws InterruptedException {
    WebElement submitNamesButtonElement = findElement(
        By.xpath("//button[@id='game-names']"));

    logger.info("Clicking on the submit names button in the dashboard board view");
    submitNamesButtonElement.click();

    Thread.sleep(120);
  }

  boolean dashboardAlertExists() {
    logger.info("Checking if the alert message has shown up in dashboard view");

    // try to find the element
    try {
      findElement(By.xpath("//div[./strong[text()='Illegal moves:']]"));
    } catch (NoSuchElementException e) {
      logger.info("Couldn't find the alert message in dashboard view");
      return false;
    }

    logger.info("Found the alert message in dashboard view");
    return true;
  }

  List<Integer> dashboardAlertGameIds() {
    WebElement alertElement = findElement(
        By.xpath("//div[./strong[text()='Illegal moves:']]"));

    logger.info("Parsing the alert text, getting game ids");

    String alertText = alertElement.getText();

    List<Integer> gameIds = new LinkedList<>();

    Matcher m = Pattern.compile("game: (\\d+)").matcher(alertText);

    while (m.find())
      gameIds.add(Integer.parseInt(m.group(1)));

    logger.info("Found {} game ids: {}", gameIds.size(), gameIds);
    return gameIds;
  }

  Integer dashboardAlertGameIllegalMove(int gameId) {
    WebElement alertElement = findElement(
        By.xpath("//div[./strong[text()='Illegal moves:']]"));

    logger.info("Parsing the alert text, getting first illegal move for game with id {}", gameId);

    String alertText = alertElement.getText();

    Matcher m = Pattern
        .compile(String.format("game: %d, move: (\\d+)", gameId))
        .matcher(alertText);

    if (m.find()) {
      if (m.groupCount() != 1) {
        logger.info("The game is listed in the alert, but without move.");
        return null;
      }

      int move = Integer.parseInt(m.group(1));

      logger.info("Found the first illegal move: {}", move);

      return move;
    }

    logger.info("The game isn't listed in the alert.");
    return null;
  }

  boolean checkHistoryContainsGameId(int gameId) throws InterruptedException {
    logger.info("Checking if history has a game with gameId={}", gameId);

    for (int visibleGameId : historyGetVisibleGameIds())
      if (visibleGameId == gameId) return true;

    return false;
  }

  void historyClickGame(int gameId) throws InterruptedException {
    goToHistory();

    WebElement gameLinkElement = findElement(
        By.xpath(String.format(
            "//tbody[@id='table-body']/*/td[contains(@id, 'column-0')]/a[text()='%d']",
            gameId)));

    logger.info("Clicking on the game with gameId={}", gameId);
    gameLinkElement.click();

    Thread.sleep(120);
  }

  int historyCurrentGameGetGameId() {
    logger.info("Finding the gameId of the currently open game view.");

    WebElement gameIdElement = findElement(
        By.xpath("//span[../span[@class='h2' and text()='Game id:'] and text()!='Game id:' and not(@class)]"));

    return Integer.parseInt(gameIdElement.getText());
  }

  void historyGoToGameView(int gameId) throws InterruptedException {
    goToHistory();

    assertTrue(checkHistoryContainsGameId(gameId));

    historyClickGame(gameId);

    assertEquals(gameId, historyCurrentGameGetGameId());
  }

  void historyCurrentGameSetWhiteName(String whiteName) {
    WebElement whiteNameFieldElement = findElement(
        By.xpath("//input[@name='white-name']"));

    logger.info("Entering {} in the name field of the white player", whiteName);
    whiteNameFieldElement.sendKeys(whiteName, Keys.ENTER);
  }

  void historyCurrentGameSetBlackName(String blackName) {
    WebElement blackNameFieldElement = findElement(
        By.xpath("//input[@name='black-name']"));

    logger.info("Entering {} in the name field of the black player", blackName);
    blackNameFieldElement.sendKeys(blackName, Keys.ENTER);
  }

  String historyCurrentGameGetWhiteName() {
    logger.info("Finding the name of the white player in the open game view in history.");

    WebElement whiteNameFieldElement = findElement(
        By.xpath("//input[@name='white-name']"));

    return whiteNameFieldElement.getAttribute("value");
  }

  String historyCurrentGameGetBlackName() {
    logger.info("Finding the name of the black player in the open game view in history.");

    WebElement blackNameFieldElement = findElement(
        By.xpath("//input[@name='black-name']"));

    return blackNameFieldElement.getAttribute("value");
  }

  void historyCurrentGameClickSubmitNamesButton() throws InterruptedException {
    WebElement submitNamesButtonElement = findElement(
        By.xpath("//button[@id='game-names']"));

    logger.info("Clicking on the submit names button in the history game view");
    submitNamesButtonElement.click();

    Thread.sleep(120);
  }

  boolean checkHistoryGameMoveCountEquals(int gameId, int expectedMoveCount) throws InterruptedException {
    logger.info("Checking if a game with gameId={} on dashboard has {} moves",
        gameId, expectedMoveCount);

    return expectedMoveCount == historyGetGameMoveCount(gameId);
  }

  List<Integer> historyGetVisibleGameIds() throws InterruptedException {
    goToHistory();

    List<WebElement> gameIdElements = findElements(
        By.xpath("//tbody[@id='table-body']/*/td[1]/a"));

    List<Integer> gameIds = new ArrayList<>(gameIdElements.size());

    for (WebElement element : gameIdElements)
      gameIds.add(Integer.parseInt(element.getText()));

    return gameIds;
  }

  int historyGetGameMoveCount(int gameId) throws InterruptedException {
    goToHistory();

    WebElement moveCount = findElement(
        By.xpath(String.format(
            "//tbody[@id='table-body']/*/td[1]/a[text()='%d']/../../td[5]/a",
            gameId)));

    return Integer.parseInt(moveCount.getText());
  }

}
