package org.chessmove.server;


import com.google.gson.Gson;
import org.chessmove.server.model.dto.MoveDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SystemTestUseCase6 extends SeleniumTest {

  @LocalServerPort
  int port;

  private int boardId;

  private List<MoveDto> legalMoves;
  private String[] legalFromStrings = {"a2", "a7", "b2", "b7", "c2", "c7", "d2", "d7"};
  private String[] legalToStrings = {"a4", "a5", "b3", "b6", "c4", "c5", "d3", "d6"};

  private List<MoveDto> illegalMoves;
  private String[] illegalFromStrings = {"e2", "e7", "f2", "f7"};
  private String[] illegalToStrings = {"e7", "e2", "a1", "a8"};

  @Before
  public void before() throws IOException, InterruptedException {
    baseUriString = baseUriStringPrefix + port;

    gson = new Gson();

    legalMoves = assembleMoveDTOs(legalFromStrings, legalToStrings);
    illegalMoves = assembleMoveDTOs(illegalFromStrings, illegalToStrings);

    createGames();
  }

  /**
   * <b>Preconditions:</b>
   * At least one game is being played.
   */
  @Test
  public void test() throws InterruptedException, IOException {
    boardId = 5;


    // The referee is on the dashboard, seeing a listing of in-progress games or is viewing a current game.
    goToDashboard();

    assertTrue(checkDashboardContainsBoardId(boardId));

    int gameId = dashboardFindBoardGameId(boardId);

    goToDashboard();


    // A board sends a chess-illegal move to the server
    apiMakeMoves(boardId, illegalMoves);


    // Postconditions: An alert is displayed to the referee, indicating that an illegal move is made
    assertTrue(dashboardAlertExists());

    assertTrue(dashboardAlertGameIds().contains(gameId));

    assertEquals(
        Integer.valueOf(legalMoves.size() + 1),
        dashboardAlertGameIllegalMove(gameId));
  }

  private void createGames() throws IOException, InterruptedException {
    for (int i = 1; i < 9; i++) {
      apiStartGame(i);

      apiMakeMoves(i, legalMoves);
    }
  }
}
