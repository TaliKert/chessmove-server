package org.chessmove.server;

import com.google.gson.Gson;
import org.chessmove.server.model.dto.MoveDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SystemTestUseCase12 extends SeleniumTest {

  @LocalServerPort
  int port;

  private int boardId;

  private List<MoveDto> sampleMoves;
  private String[] sampleFromStrings =
      {"a2", "a7", "b2", "b7", "c2", "c7", "d2", "d7", "e2", "e7", "f2", "f7", "g2", "g7", "h2", "h7"};
  private String[] sampleToStrings =
      {"a4", "a5", "b3", "b6", "c4", "c5", "d3", "d6", "e4", "e5", "f3", "f6", "g4", "g5", "h3", "h6"};

  private List<MoveDto> finishingMoves;
  private String[] finishingFromStrings = {"e1", "e1", "e1"};
  private String[] finishingToStrings = {"e1", "e1", "e1"};


  @Before
  public void before() throws IOException, InterruptedException {
    baseUriString = baseUriStringPrefix + port;

    gson = new Gson();

    // Construct some ID-less move mock data
    sampleMoves = assembleMoveDTOs(sampleFromStrings, sampleToStrings);

    finishingMoves = assembleMoveDTOs(finishingFromStrings, finishingToStrings);

    createGames();
  }


  /**
   * <b>Preconditions:</b>
   * A game has been started.
   */
  @Test
  public void testDashboard() throws InterruptedException {
    boardId = 5;

    String whiteName = "Breath Taking";
    String blackName = "Keanu Reeves";


    // Player selects an active game
    dashboardGoToBoardView(boardId);


    // A name for player 1 is inserted
    dashboardCurrentBoardSetBlackName(blackName);


    // A name for player 2 is inserted
    dashboardCurrentBoardSetWhiteName(whiteName);

    dashboardCurrentBoardClickSubmitNamesButton();


    // Postconditions: The names are saved and are displayed next to other game-related information
    dashboardGoToBoardView(boardId);

    assertEquals(whiteName, dashboardCurrentBoardGetWhiteName());
    assertEquals(blackName, dashboardCurrentBoardGetBlackName());
  }

  /**
   * <b>Preconditions:</b>
   * A game has been started.
   */
  @Test
  public void testHistory() throws InterruptedException, IOException {
    boardId = 5;

    String whiteName = "Breath Taking";
    String blackName = "Keanu Reeves.";

    assertTrue(checkDashboardContainsBoardId(boardId));

    int gameId = dashboardFindBoardGameId(boardId);

    makeFinishingMoves();

    // The game has ended and the player goes to the history page and selects the game from there
    historyGoToGameView(gameId);

    // A name for player 1 is inserted
    historyCurrentGameSetWhiteName(whiteName);


    // A name for player 2 is inserted
    historyCurrentGameSetBlackName(blackName);

    historyCurrentGameClickSubmitNamesButton();


    // The names are saved and are displayed next to other game-related information
    historyGoToGameView(gameId);

    assertEquals(whiteName, historyCurrentGameGetWhiteName());
    assertEquals(blackName, historyCurrentGameGetBlackName());
  }

  private void createGames() throws IOException, InterruptedException {
    for (int i = 1; i < 9; i++) {
      apiStartGame(i);

      apiMakeMoves(i, sampleMoves);
    }
  }

  private void makeFinishingMoves() throws IOException, InterruptedException {
    for (MoveDto move : finishingMoves)
      apiMakeMove(boardId, move);
  }
}
