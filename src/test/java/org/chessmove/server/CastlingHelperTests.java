package org.chessmove.server;

import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import org.chessmove.server.helper.CastlingHelper;
import org.chessmove.server.model.Game;
import org.chessmove.server.service.MoveService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;
import java.util.Collections;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CastlingHelperTests {

  @InjectMocks
  @Spy
  CastlingHelper castlingHelper;

  @Mock
  MoveService moveService;

  @Test
  public void testRookBeforeKingGetsDeleted() {
    Game game = spy(new Game());
    org.chessmove.server.model.Move mockMove = mock(org.chessmove.server.model.Move.class);
    doReturn(Collections.singletonList(mockMove)).when(game).getMoves();
    Board board = new Board();
    board.loadFromFen("8/8/8/8/8/8/8/R3K3 w KQ -");
    board.doMove(new Move(Square.A1, Square.D1));

    when(moveService.getBoard(game.getMoves())).thenReturn(board);

    castlingHelper.handleCastlingForgiving(
        new org.chessmove.server.model.Move(game, "E1", "C1", Instant.now()), game
    );

    verify(mockMove).setDeleted(true);
  }
}
