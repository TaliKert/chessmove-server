package org.chessmove.server;

import com.google.gson.Gson;
import org.chessmove.server.model.dto.MoveDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SystemTestUseCase2 extends SeleniumTest {

  @LocalServerPort
  int port;
  private List<Integer> boardIds;

  private List<MoveDto> sampleMoves;
  private String[] sampleFromStrings =
      {"a2", "a7", "b2", "b7", "c2", "c7", "d2", "d7", "e2", "e7", "f2", "f7", "g2", "g7", "h2", "h7"};
  private String[] sampleToStrings =
      {"a4", "a5", "b3", "b6", "c4", "c5", "d3", "d6", "e4", "e5", "f3", "f6", "g4", "g5", "h3", "h6"};

  @Before
  public void before() {
    baseUriString = "http://localhost:" + port;

    boardIds = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);

    gson = new Gson();

    // Construct some ID-less move mock data
    sampleMoves = assembleMoveDTOs(sampleFromStrings, sampleToStrings);
  }

  @Test
  public void test() throws IOException, InterruptedException {
    driver.get(baseUriString);

    // Start some games
    createGames();

    // Check that all 8 started games are showing
    for (int i = 1; i <= 8; i++) {
      int finalI = i;
      wait.until(var -> driver
          .findElement(By.xpath(String.format("//div[@data-boardid=\"%d\"]", finalI)))
          .isDisplayed());
    }

    // Open the game with boardId 1
    driver.get(baseUriString + "/game/1");

    // Confirm that a single game has 0 moves
    By byMoveCountElement = By.xpath("//div[span[@class='greyTxt' and text()='move: ']]/span[2]");

    wait.until(var -> driver.findElement(byMoveCountElement/*By.xpath("//span[text() = '00']")*/));
    assertEquals(
        0,
        Integer.parseInt(driver.findElement(byMoveCountElement).getText()));

    // Send some moves via API
    postMoves();

    // Confirm that the game now has 16 moves
    wait.until(var -> driver.findElement(byMoveCountElement/*By.xpath("//span[text() = '16']")*/));

    // Navigate back to the front page
    driver.get(baseUriString);

    wait.until(var -> driver
        .findElement(By.xpath("//div[@data-boardid='1']")/*By.xpath("//div[@data-boardid=\"1\"]")*/)
        .isDisplayed());

    // Confirm that all 8 games have 16 moves
    assertEquals(8,
        driver
            .findElements(By.xpath("//span[@class='greyTxt' and contains(text()[2], '16')]"))
            .size());
  }

  private void createGames() throws IOException, InterruptedException {
    for (int boardId : boardIds) {
      apiStartGame(boardId);

      // keeping the previous code just in case
      /*assertEquals(
          HttpStatus.SC_CREATED,
          HttpClientBuilder
              .create()
              .build()
              .execute(new HttpPost(baseUriString + "/api/start/" + boardId))
              .getStatusLine()
              .getStatusCode());*/
    }
  }

  private void postMoves() throws IOException, InterruptedException {
    for (int boardId : boardIds)
      for (MoveDto sampleMove : sampleMoves) {
        Thread.sleep(120);

        apiMakeMove(boardId, sampleMove);

        // keeping the previous code just in case
        /*sampleMove.setBoardId("" + boardId);

        HttpPost movePost = new HttpPost(baseUriString + "/api/move");
        movePost.setEntity(new StringEntity(gson.toJson(sampleMove)));
        movePost.setHeader("Content-type", "application/json");

        assertEquals(
            HttpStatus.SC_CREATED,
            HttpClientBuilder
                .create()
                .build()
                .execute(movePost)
                .getStatusLine()
                .getStatusCode());

        Thread.sleep(120);*/
      }
  }
}
