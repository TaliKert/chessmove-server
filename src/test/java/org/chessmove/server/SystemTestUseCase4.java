package org.chessmove.server;

import com.google.gson.Gson;
import org.chessmove.server.model.dto.MoveDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SystemTestUseCase4 extends SeleniumTest {

  @LocalServerPort
  int port;

  private List<Integer> boardIds;

  private List<MoveDto> sampleMoves;
  private String[] sampleFromStrings =
      {"a2", "a7", "b2", "b7", "c2", "c7", "d2", "d7", "e2", "e7", "f2", "f7", "g2", "g7", "h2", "h7"};
  private String[] sampleToStrings =
      {"a4", "a5", "b3", "b6", "c4", "c5", "d3", "d6", "e4", "e5", "f3", "f6", "g4", "g5", "h3", "h6"};

  @Before
  public void before() {
    baseUriString = "http://localhost:" + port;

    boardIds = new ArrayList<>(8);
    for (int i = 1; i < 9; i++) boardIds.add(i);

    gson = new Gson();

    // Construct some ID-less move mock data
    sampleMoves = assembleMoveDTOs(sampleFromStrings, sampleToStrings);
  }

  @Test
  public void test() throws IOException, InterruptedException {
    // create some past games
    createPastGames();

    // The user selects “History”.
    driver.get(baseUriString + "/history");

    Thread.sleep(4000);

    // An ordered list of played games is displayed
    List<WebElement> foundGameIds = driver.findElements(
        By.xpath("//td[1]/a"));

    assertEquals(boardIds.size(), foundGameIds.size());

    int id = -1;
    for (WebElement element : foundGameIds) {
      int thisId = Integer.parseInt(element.getText());

      assertTrue(thisId < id || id < 0);

      id = thisId;
    }

    // The user selects the game he wishes to view
    WebElement gameHistoryLink = foundGameIds.get(0);

    gameHistoryLink.click();

    Thread.sleep(4000);

    By byMoveCountElement = By.xpath("//div[span[@class='greyTxt' and text()='move: ']]/span[2]");

    Consumer<Integer> assertMoveCounter = integer ->
        assertEquals((int) integer,
            Integer.parseInt(driver.findElement(byMoveCountElement).getText()));

    WebElement previousStateButton = driver.findElement(By.xpath("//button[@id='move-back']"));
    for (int i = sampleMoves.size(); i > 0; i--) {
      assertMoveCounter.accept(i);

      previousStateButton.click();
    }

    assertMoveCounter.accept(0);

    WebElement nextStateButton = driver.findElement(By.xpath("//button[@id='move-forward']"));
    for (int i = 1; i <= sampleMoves.size(); i++) {
      nextStateButton.click();

      assertMoveCounter.accept(i);
    }
  }

  private void createPastGames() throws IOException, InterruptedException {
    // keeping the previous code just in case
    String uriStartTemplate = baseUriString + "/api/start/%s";
    String uriMoveTemplate = baseUriString + "/api/move";
    String uriEndTemplate = baseUriString + "/xhr/game/%s/end/draw";

    for (int boardId : boardIds) {
      apiStartGame(boardId);

      // keeping the previous code just in case
      /*assertEquals(
          HttpStatus.SC_CREATED,
          HttpClientBuilder
              .create()
              .build()
              .execute(new HttpPost(String.format(uriStartTemplate, boardId)))
              .getStatusLine()
              .getStatusCode());*/
    }

    for (int boardId : boardIds) {
      for (MoveDto moveDto : sampleMoves) {
        Thread.sleep(120);

        apiMakeMove(boardId, moveDto);

        // keeping the previous code just in case
        /*Thread.sleep(120);

        moveDto.setBoardId(boardId);

        HttpPost post = new HttpPost(uriMoveTemplate);
        post.setEntity(new StringEntity(gson.toJson(moveDto)));
        post.setHeader("Content-type", "application/json");

        assertEquals(
            HttpStatus.SC_CREATED,
            HttpClientBuilder
                .create().build().execute(post).getStatusLine().getStatusCode());*/
      }

      Thread.sleep(120);

      xhrEndGame(boardId);

      // keeping the previous code just in case
      /*assertEquals(
          HttpStatus.SC_OK,
          HttpClientBuilder
              .create()
              .build()
              .execute(new HttpPost(String.format(uriEndTemplate, boardId)))
              .getStatusLine()
              .getStatusCode());*/
    }
  }
}
