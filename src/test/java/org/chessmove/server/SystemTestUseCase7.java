package org.chessmove.server;

import com.google.gson.Gson;
import org.chessmove.server.model.dto.MoveDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SystemTestUseCase7 extends SeleniumTest {

  @LocalServerPort
  int port;

  private int boardId;

  private List<MoveDto> sampleMoves;
  private String[] sampleFromStrings =
      {"a2", "a7", "b2", "b7", "c2", "c7", "d2"};
  private String[] sampleToStrings =
      {"a4", "a5", "b3", "b6", "c4", "c5", "d3"};

  @Before
  public void before() throws IOException, InterruptedException {
    baseUriString = baseUriStringPrefix + port;

    gson = new Gson();

    // Construct some ID-less move mock data
    sampleMoves = assembleMoveDTOs(sampleFromStrings, sampleToStrings);

    createGames();
  }

  /**
   * <b>Preconditions:</b>
   * At least one game is being played.
   */
  @Test
  public void test() throws InterruptedException {
    boardId = 5;


    // The referee is on the dashboard, seeing a listing of in-progress games
    goToDashboard();

    assertTrue(checkDashboardContainsBoardId(boardId));


    // The referee clicks on the game he/she wishes to revert
    dashboardClickBoard(boardId);

    assertEquals(boardId, dashboardCurrentBoardId());

    assertEquals(sampleMoves.size(), dashboardCurrentBoardMoveCount());


    // The referee clicks on the move back button
    dashboardCurrentBoardClickPreviousState();

    assertEquals(sampleMoves.size() - 1, dashboardCurrentBoardCurrentStateMoveCount());


    // The referee clicks on the revert to this state button
    dashboardCurrentBoardClickRevertState();


    // Postconditions: The game is reverted to the wished state in both the graphical view and the database
    assertEquals(boardId, dashboardCurrentBoardId());

    assertEquals(sampleMoves.size() - 1, dashboardCurrentBoardMoveCount());
  }

  private void createGames() throws IOException, InterruptedException {
    for (int i = 1; i < 9; i++) {
      apiStartGame(i);

      apiMakeMoves(i, sampleMoves);
    }
  }
}
