package org.chessmove.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.chessmove.server.controller.api.ajax.AjaxController;
import org.chessmove.server.engine.Stockfish;
import org.chessmove.server.model.Game;
import org.chessmove.server.model.dto.GameDto;
import org.chessmove.server.repository.GameRepository;
import org.chessmove.server.service.GameService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AjaxController.class)
public class AjaxControllerTests {

  /**
   * Just for the sake of universality.
   */
  private String map = "/xhr";

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  GameService gameService;

  @MockBean
  GameRepository gameRepository;

  @MockBean
  Stockfish stockfishEngine;

  @Test
  public void gamesReturnListOfGames() throws Exception {
    // preparation
    int gameCount = 10;

    List<Game> gamesList = new ArrayList<>(gameCount);
    List<GameDto> gameDTOsList = new ArrayList<>(gameCount);

    for (int i = 0; i < gameCount; i++) {
      Game game = new Game("" + i);
      gamesList.add(game);
      gameDTOsList.add(new GameDto(game));
    }

    String expectedJSON = new ObjectMapper().writeValueAsString(gameDTOsList);

    doReturn(gamesList)
        .when(gameService).inProgressGames();

    for (int i = 0; i < gamesList.size(); i++)
      doReturn(gameDTOsList.get(i))
          .when(gameService).gameToGameDto(gamesList.get(i));

    // testing
    MvcResult result = this.mockMvc
        .perform(get("{map}/games", map))
        .andExpect(status().isOk()).andReturn();

    assertEquals(expectedJSON, result.getResponse().getContentAsString());

    // verification
    verify(gameService, times(1)).inProgressGames();

    for (Game game : gamesList)
      verify(gameService, times(1)).gameToGameDto(game);
  }

  @Test
  public void gamesReturnListOfGames_noGames() throws Exception {
    // preparation
    int gameCount = 10;

    List<Game> gamesList = new ArrayList<>(gameCount);

    for (int i = 0; i < gameCount; i++)
      gamesList.add(new Game("" + i));

    String expectedJSON = new ObjectMapper().writeValueAsString(new LinkedList<>());

    doReturn(new LinkedList<Game>())
        .when(gameService).inProgressGames();

    for (Game game : gamesList)
      doReturn(null)
          .when(gameService).gameToGameDto(game);

    // testing
    MvcResult result = this.mockMvc
        .perform(get("{map}/games", map))
        .andExpect(status().isOk()).andReturn();

    assertEquals(expectedJSON, result.getResponse().getContentAsString());

    // verification
    verify(gameService, times(1)).inProgressGames();

    for (Game game : gamesList)
      verify(gameService, never()).gameToGameDto(game);
  }

  @Test
  public void gameReturnGame() throws Exception {
    // preparation
    int gameCount = 10;
    int requestedGame = 5;

    List<Game> gamesList = new ArrayList<>(gameCount);
    List<GameDto> gameDTOsList = new ArrayList<>(gameCount);

    for (int i = 0; i < gameCount; i++) {
      Game game = new Game("" + i);
      gamesList.add(game);
      gameDTOsList.add(new GameDto(game));
    }

    String expectedJSON = new ObjectMapper().writeValueAsString(gameDTOsList.get(requestedGame));

    for (int i = 0; i < gamesList.size(); i++) {
      doReturn(Optional.of(gamesList.get(i)))
          .when(gameRepository).findById(i);
      doReturn(gameDTOsList.get(i))
          .when(gameService).gameToGameDto(gamesList.get(i));
    }

    // testing
    MvcResult result = this.mockMvc
        .perform(get("{map}/game/{gameId}",
            map, requestedGame))
        .andExpect(status().isOk()).andReturn();

    assertEquals(expectedJSON, result.getResponse().getContentAsString());

    // verification
    verify(gameRepository, times(1)).findById(requestedGame);
    verify(gameService, times(1))
        .gameToGameDto(gamesList.get(requestedGame));

    for (int i = 0; i < gamesList.size(); i++)
      if (i != requestedGame) {
        verify(gameRepository, never()).findById(i);
        verify(gameService, never()).gameToGameDto(gamesList.get(i));
      }
  }


  @Test
  public void gameReturnGame_noGames() throws Exception {
    // preparation
    int gameCount = 10;
    int requestedGame = 5;

    List<Game> gamesList = new ArrayList<>(gameCount);

    for (int i = 0; i < gameCount; i++)
      gamesList.add(new Game("" + i));

    for (int i = 0; i < gamesList.size(); i++) {
      doReturn(Optional.empty())
          .when(gameRepository).findById(i);
      doReturn(null)
          .when(gameService).gameToGameDto(gamesList.get(i));
    }

    // testing
    this.mockMvc
        .perform(get("{map}/game/{gameId}",
            map, requestedGame))
        .andExpect(status().isNotFound());

    // verification
    verify(gameRepository, times(1)).findById(requestedGame);

    for (int i = 0; i < gamesList.size(); i++) {
      verify(gameService, never()).gameToGameDto(gamesList.get(i));

      if (i != requestedGame)
        verify(gameRepository, never()).findById(i);
    }
  }

  @Test
  public void historyReturnAllPastGames() throws Exception {
    // preparation
    int gameCount = 10;

    List<Integer> pastGameIds = Arrays.asList(1, 6, 8, 9);

    List<Game> gamesList = new ArrayList<>(gameCount);
    List<GameDto> gameDTOsList = new ArrayList<>(gameCount);

    List<Game> pastGamesList = new ArrayList<>(pastGameIds.size());
    List<GameDto> pastGameDTOsList = new ArrayList<>(pastGameIds.size());

    for (int i = 0; i < gameCount; i++) {
      Game game = new Game("" + i);
      gamesList.add(game);

      GameDto gameDTO = new GameDto(game);
      gameDTOsList.add(gameDTO);

      if (pastGameIds.contains(i)) {
        pastGamesList.add(game);
        pastGameDTOsList.add(gameDTO);
      }
    }

    String expectedJSON = new ObjectMapper().writeValueAsString(pastGameDTOsList);

    doReturn(pastGamesList)
        .when(gameService).pastGames();

    for (int i = 0; i < gamesList.size(); i++)
      doReturn(gameDTOsList.get(i))
          .when(gameService).gameToGameDto(gamesList.get(i));

    // testing
    MvcResult result = this.mockMvc
        .perform(get("{map}/history", map))
        .andExpect(status().isOk()).andReturn();

    assertEquals(expectedJSON, result.getResponse().getContentAsString());

    // verification
    verify(gameService, times(1)).pastGames();

    for (Game game : gamesList)
      verify(gameService,
          times(pastGamesList.contains(game) ? 1 : 0))
          .gameToGameDto(game);
  }

  @Test
  public void historyReturnAllPastGames_noPastGames() throws Exception {
    // preparation
    int gameCount = 10;

    List<Integer> pastGameIds = Collections.emptyList();

    List<Game> gamesList = new ArrayList<>(gameCount);
    List<GameDto> gameDTOsList = new ArrayList<>(gameCount);

    for (int i = 0; i < gameCount; i++) {
      Game game = new Game("" + i);
      gamesList.add(game);
      gameDTOsList.add(new GameDto(game));
    }

    String expectedJSON = new ObjectMapper().writeValueAsString(new LinkedList<GameDto>());

    doReturn(new LinkedList<Game>())
        .when(gameService).pastGames();

    for (int i = 0; i < gamesList.size(); i++)
      doReturn(gameDTOsList.get(i))
          .when(gameService).gameToGameDto(gamesList.get(i));

    // testing
    MvcResult result = this.mockMvc
        .perform(get("{map}/history", map))
        .andExpect(status().isOk()).andReturn();

    assertEquals(expectedJSON, result.getResponse().getContentAsString());

    // verification
    verify(gameService, times(1)).pastGames();

    for (Game game : gamesList)
      verify(gameService, never()).gameToGameDto(game);
  }

  @Test
  public void historyReturnAllPastGames_noGames() throws Exception {
    // preparation
    int gameCount = 10;

    List<Game> gamesList = new ArrayList<>(gameCount);

    for (int i = 0; i < gameCount; i++)
      gamesList.add(new Game("" + i));

    String expectedJSON = new ObjectMapper().writeValueAsString(new LinkedList<GameDto>());

    doReturn(new LinkedList<Game>())
        .when(gameService).pastGames();

    for (Game game : gamesList)
      doReturn(null)
          .when(gameService).gameToGameDto(game);

    // testing
    MvcResult result = this.mockMvc
        .perform(get("{map}/history", map))
        .andExpect(status().isOk()).andReturn();

    assertEquals(expectedJSON, result.getResponse().getContentAsString());

    // verification
    verify(gameService, times(1)).pastGames();

    for (Game game : gamesList)
      verify(gameService, never()).gameToGameDto(game);
  }

  @Test
  public void revertRequestsGameAndCallsGameServiceRevertMove() throws Exception {
    // preparation
    int gameId = 1337;
    int moveIdx = 24;

    Game game = new Game("" + gameId);

    doReturn(Optional.of(game))
        .when(gameRepository).findById(gameId);

    // testing
    this.mockMvc
        .perform(post("{map}/game/{gameId}/revert/{moveIdx}",
            map, gameId, moveIdx))
        .andExpect(status().isOk());

    // verification
    verify(gameRepository, times(1)).findById(gameId);
    verify(gameService, times(1))
        .revertMove(game, moveIdx);
  }

  @Test
  public void revertRequestsGameAndCallsGameServiceRevertMove_noGame() throws Exception {
    // preparation
    int gameId = 1337;
    int moveIdx = 24;

    Game game = new Game("" + gameId);

    doReturn(Optional.empty())
        .when(gameRepository).findById(gameId);

    // testing
    this.mockMvc
        .perform(post("{map}/game/{gameId}/revert/{moveIdx}",
            map, gameId, moveIdx))
        .andExpect(status().isNotFound());

    // verification
    verify(gameRepository, times(1)).findById(gameId);
    verify(gameService, never()).revertMove(game, moveIdx);
  }

  @Test
  public void setWinnerRequestsGameAndCallsGameServiceEndGameInProgress() throws Exception {
    // preparation
    int gameId = 1337;
    String result = "draw";

    Game game = new Game("" + gameId);

    doReturn(Optional.of(game))
        .when(gameRepository).findById(gameId);

    // testing
    this.mockMvc
        .perform(post("{map}/game/{gameId}/end/{result}",
            map, gameId, result))
        .andExpect(status().isOk());

    // verification
    verify(gameRepository, times(1)).findById(gameId);
    verify(gameService, times(1))
        .endGameInProgress(game, result);
  }

  @Test
  public void setWinnerRequestsGameAndCallsGameServiceEndGameInProgress_noGame() throws Exception {
    // preparation
    int gameId = 1337;
    String result = "draw";

    Game game = new Game("" + gameId);

    doReturn(Optional.empty())
        .when(gameRepository).findById(gameId);

    // testing
    this.mockMvc
        .perform(post("{map}/game/{gameId}/end/{result}",
            map, gameId, result))
        .andExpect(status().isNotFound());

    // verification
    verify(gameRepository, times(1)).findById(gameId);
    verify(gameService, never()).endGameInProgress(game, result);
  }
}
