package org.chessmove.server;

import com.google.gson.Gson;
import org.chessmove.server.model.dto.MoveDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SystemTestUseCase1 extends SeleniumTest {

  @LocalServerPort
  int port;

  private int boardId;

  private int moveCount = 0;

  private List<MoveDto> sampleMoves;
  private String[] sampleFromStrings =
      {"a2", "a7", "b2", "b7", "c2", "c7", "d2", "d7", "e2", "e7", "f2", "f7", "g2", "g7", "h2", "h7"};
  private String[] sampleToStrings =
      {"a4", "a5", "b3", "b6", "c4", "c5", "d3", "d6", "e4", "e5", "f3", "f6", "g4", "g5", "h3", "h6"};

  private List<MoveDto> finishingMoves;
  private String[] finishingFromStrings = {"e1", "e1", "e1"};
  private String[] finishingToStrings = {"e1", "e1", "e1"};

  @Before
  public void before() {
    baseUriString = baseUriStringPrefix + port;

    gson = new Gson();

    // Construct some ID-less move mock data
    sampleMoves = assembleMoveDTOs(sampleFromStrings, sampleToStrings);
        /*new ArrayList<>(sampleFromStrings.length);
    for (int i = 0; i < sampleFromStrings.length; i++) {
      MoveDto moveDto = new MoveDto();
      moveDto.setFrom(sampleFromStrings[i]);
      moveDto.setTo(sampleToStrings[i]);

      sampleMoves.add(moveDto);
    }*/

    finishingMoves = assembleMoveDTOs(finishingFromStrings, finishingToStrings);
  /*new ArrayList<>(finishingFromStrings.length);
    for (int i = 0; i < finishingFromStrings.length; i++) {
      MoveDto moveDto = new MoveDto();
      moveDto.setFrom(finishingFromStrings[i]);
      moveDto.setTo(finishingToStrings[i]);

      finishingMoves.add(moveDto);
    }*/
  }

  /**
   * <b>Preconditions:</b>
   * The system is listening to the chess board.
   */
  @Test
  public void test() throws IOException, InterruptedException {
    boardId = 10;

    assertFalse(checkDashboardContainsBoardId(boardId));


    // The player makes a combination on the board that indicates the start of the game.
    apiStartGame(boardId);

    assertTrue(checkDashboardContainsBoardId(boardId));

    assertTrue(checkDashboardBoardMoveCountEquals(boardId, moveCount));

    int gameId = dashboardFindBoardGameId(boardId);


    // Some amount of moves are made.
    makeMoves(sampleMoves);


    // The player makes a combination on the board that indicates the end of the game.
    makeFinishingMoves(finishingMoves);

    assertFalse(checkDashboardContainsBoardId(boardId));


    // Postconditions: The game is saved and viewable on the history page.
    assertTrue(checkHistoryContainsGameId(gameId));

    assertTrue(checkHistoryGameMoveCountEquals(gameId, moveCount));
  }

  private void makeMoves(List<MoveDto> moves) throws IOException, InterruptedException {
    for (MoveDto move : moves) {
      apiMakeMove(boardId, move);

      assertTrue(checkDashboardBoardMoveCountEquals(
          boardId,
          ++moveCount));
    }
  }

  private void makeFinishingMoves(List<MoveDto> moves) throws IOException, InterruptedException {
    int movesLeft = moves.size();
    for (MoveDto move : moves) {
      apiMakeMove(boardId, move);

      movesLeft--;

      if (checkDashboardContainsBoardId(boardId))
        assertTrue(checkDashboardBoardMoveCountEquals(
            boardId,
            moveCount));
      else
        assertEquals(0, movesLeft);
    }
  }
}
