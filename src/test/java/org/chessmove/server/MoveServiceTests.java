package org.chessmove.server;

import org.chessmove.server.helper.CastlingHelper;
import org.chessmove.server.model.Game;
import org.chessmove.server.model.Move;
import org.chessmove.server.model.dto.MoveDto;
import org.chessmove.server.repository.MoveRepository;
import org.chessmove.server.service.GameService;
import org.chessmove.server.service.MoveService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class MoveServiceTests {

  @Mock
  MoveRepository moveRepository;

  @InjectMocks
  @Spy
  MoveService moveService;

  @Mock
  GameService gameService;

  @Spy
  CastlingHelper castlingHelper;

  @Test
  public void fenList_legalMoves() {
    Game game = new Game("a1b2c");
    List<Move> moves = new ArrayList<>(Arrays.asList(
        new Move(game, "e2", "e4", null),
        new Move(game, "e7", "e5", null),
        new Move(game, "d2", "d4", null),
        new Move(game, "e5", "d4", null)));
    LinkedList<String> fens = new LinkedList<>(Arrays.asList(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1",
        "rnbqkbnr/pppp1ppp/8/4p3/4P3/8/PPPP1PPP/RNBQKBNR w KQkq e6 0 2",
        "rnbqkbnr/pppp1ppp/8/4p3/3PP3/8/PPP2PPP/RNBQKBNR b KQkq d3 0 2",
        "rnbqkbnr/pppp1ppp/8/8/3pP3/8/PPP2PPP/RNBQKBNR w KQkq - 0 3"));
    doReturn(moves).when(moveRepository).findAllByGameAndDeletedIsFalseOrderByTimeAsc(game);

    LinkedList<String> result = moveService.getFenList(game);

    assertNull(result.get(0));
    assertEquals(fens.get(0), result.get(1));
    assertEquals(fens.get(1), result.get(2));
    assertEquals(fens.get(2), result.get(3));
    assertEquals(fens.get(3), result.get(4));
    assertEquals(fens.get(4), result.get(5));
  }

  @Test
  public void fenList_illegalMoves() {
    Game game = new Game("a1b2c");
    List<Move> moves = new ArrayList<>(Arrays.asList(
        new Move(game, "e2", "e6", null),
        new Move(game, "e7", "e5", null),
        new Move(game, "d2", "d4", null),
        new Move(game, "e5", "d4", null)));
    LinkedList<String> fens = new LinkedList<>(Arrays.asList(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        "rnbqkbnr/pppppppp/4P3/8/8/8/PPPP1PPP/RNBQKBNR b KQkq - 0 1",
        "rnbqkbnr/pppp1ppp/4P3/4p3/8/8/PPPP1PPP/RNBQKBNR w KQkq e6 0 2",
        "rnbqkbnr/pppp1ppp/4P3/4p3/3P4/8/PPP2PPP/RNBQKBNR b KQkq d3 0 2",
        "rnbqkbnr/pppp1ppp/4P3/8/3p4/8/PPP2PPP/RNBQKBNR w KQkq - 0 3"));
    doReturn(moves).when(moveRepository).findAllByGameAndDeletedIsFalseOrderByTimeAsc(game);

    LinkedList<String> result = moveService.getFenList(game);

    assertEquals("1", result.get(0));
    assertEquals(fens.get(0), result.get(1));
    assertEquals(fens.get(1), result.get(2));
    assertEquals(fens.get(2), result.get(3));
    assertEquals(fens.get(3), result.get(4));
    assertEquals(fens.get(4), result.get(5));
  }

  @Test
  public void moveIsSaved_legalMove() {
    Game game = new Game("a1b2c");
    game.setMoves(new ArrayList<>());
    MoveDto move = new MoveDto();
    move.setFrom("e2");
    move.setTo("e4");
    move.setBoardId("a1b2c");
    doReturn(game).when(gameService).findGameInProgress("a1b2c");

    moveService.handleMove(move);

    verify(moveRepository, times(1)).save(any());
  }

  @Test
  public void gameIsEnded_whiteWin() {
    Game game = spy(new Game("a1b2c"));
    List<Move> movesIncludingDeleted = new ArrayList<>(
        Arrays.asList(
            new Move(game, "e8", "e8", null),
            new Move(game, "e8", "e8", null),
            new Move(game, "e2", "e4", null)
        )
    );
    List<Move> allMoves = new ArrayList<>(
        Arrays.asList(
            new Move(game, "e2", "e4", null)
        )
    );
    doReturn(movesIncludingDeleted).when(moveRepository).findAllByGameOrderByTimeDesc(game);

    doReturn(allMoves).when(game).getMoves();

    moveService.checkGameEnd(game, new Move(null, "e8", "e8", null));

    verify(gameService, times(1)).endGameInProgress(game, "white");
  }
}
