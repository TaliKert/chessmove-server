package org.chessmove.server;

import org.chessmove.server.model.Game;
import org.chessmove.server.model.Move;
import org.chessmove.server.model.dto.GameDto;
import org.chessmove.server.repository.GameRepository;
import org.chessmove.server.repository.MoveRepository;
import org.chessmove.server.service.GameService;
import org.chessmove.server.service.MoveService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTests {

  @InjectMocks
  @Spy
  GameService gameService;

  @Mock
  GameRepository gameRepository;

  @Mock
  MoveService moveService;

  @Mock
  MoveRepository moveRepository;

  @Test
  public void gameIsInitiated_gameInProgressOnBoard() {
    Game game = new Game("a1b2c");
    doReturn(game).when(gameRepository).findGameByBoardIdEqualsAndEndTimeIsNull("a1b2c");

    gameService.initiateGame("a1b2c");

    verify(gameService).endGameInProgress(game, null);
    verify(gameRepository).save(any());
  }

  @Test
  public void gameIsInitiated_noGameInProgressOnBoard() {
    Game game = new Game("a1b2c");
    doReturn(null).when(gameRepository).findGameByBoardIdEqualsAndEndTimeIsNull("a1b2c");

    gameService.initiateGame("a1b2c");

    verify(gameService, never()).endGameInProgress(game, null);
    verify(gameRepository).save(any());
  }

  @Test
  public void gameIsEnded_inProgressGame() {
    Game game = spy(new Game("a1b2c"));

    gameService.endGameInProgress(game, "draw");

    verify(game).setResult("draw");
    verify(game).setEndTime(any(Instant.class));
  }

  @Test
  public void gameIsNotEnded_noInProgressGame() {
    Game game = spy(new Game("a1b2c"));
    game.setEndTime(Instant.now());

    gameService.endGameInProgress(game, "draw");

    verify(game, never()).setResult("draw");
    verify(game, times(1)).setEndTime(any(Instant.class));
  }

  @Test
  public void gameDtoConstructionTest() {
    Game game = new Game("a1b2c");
    LinkedList<String> fens = new LinkedList<>(Arrays.asList(
        null,
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -",
        "rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -",
        "rnbqkbnr/pppp1ppp/4p3/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -"));
    LinkedList<String> fensAssert = new LinkedList<>(fens);
    doReturn(fens).when(moveService).getFenList(game);
    doReturn(fens.size() - 1).when(moveRepository).countMovesByGameAndDeletedIsFalse(game);

    GameDto result = gameService.gameToGameDto(game);

    assertEquals(fens.getLast(), result.getPosition());
    assertEquals(fens, result.getPrevPositions());
    assertEquals(Integer.valueOf(fensAssert.size() - 1), result.getMoveCount());
    assertEquals(Integer.valueOf(-1), result.getIllegalStateIndex());
  }

  @Test
  public void gameDtoConstructionTest_IllegalMove() {
    Game game = new Game("a1b2c");
    LinkedList<String> fens = new LinkedList<>(Arrays.asList(
        "1",
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -",
        "rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -",
        "rnbqkbnr/pppp1ppp/4p3/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -"));
    LinkedList<String> fensAssert = new LinkedList<>(fens);
    doReturn(fens).when(moveService).getFenList(game);
    doReturn(fens.size() - 1).when(moveRepository).countMovesByGameAndDeletedIsFalse(game);

    GameDto result = gameService.gameToGameDto(game);

    assertEquals(fens.getLast(), result.getPosition());
    assertEquals(fens, result.getPrevPositions());
    assertEquals(Integer.valueOf(fensAssert.size() - 1), result.getMoveCount());
    assertEquals(Integer.valueOf(1), result.getIllegalStateIndex());
  }

  @Test
  public void revertedMovesAreSoftDeleted() {
    Game game = new Game("a1b2c");
    List<Move> moves = new ArrayList<>(Arrays.asList(
        new Move(game, "a2", "a3", null),
        new Move(game, "b2", "b3", null),
        new Move(game, "c2", "c3", null),
        new Move(game, "d2", "d3", null)));
    doReturn(moves).when(moveRepository).findAllByGameAndDeletedIsFalseOrderByTimeAsc(game);

    gameService.revertMove(game, 2);

    assertFalse(moves.get(0).isDeleted());
    assertFalse(moves.get(1).isDeleted());
    assertTrue(moves.get(2).isDeleted());
    assertTrue(moves.get(3).isDeleted());
  }
}
