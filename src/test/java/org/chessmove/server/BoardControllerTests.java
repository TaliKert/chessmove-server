package org.chessmove.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.chessmove.server.controller.api.board.BoardController;
import org.chessmove.server.model.dto.MoveDto;
import org.chessmove.server.service.GameService;
import org.chessmove.server.service.MoveService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BoardController.class)
public class BoardControllerTests {

  /**
   * Just for the sake of universality.
   */
  private String map = "/api";

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  GameService gameService;

  @MockBean
  MoveService moveService;

  @Test
  public void startCallsGameServiceInitiateGame() throws Exception {
    String boardId = "1337";

    this.mockMvc
        .perform(post("{map}/start/{boardId}", map, boardId))
        .andDo(print())
        .andExpect(status().isCreated());

    verify(gameService, times(1))
        .initiateGame(boardId);
  }

  @Test
  public void moveCallsMoveServiceHandleMove() throws Exception {
    MoveDto moveDto = new MoveDto();
    moveDto.boardId = "1337";
    moveDto.from = "a2";
    moveDto.to = "a3";

    String json = new ObjectMapper().writeValueAsString(moveDto);

    this.mockMvc
        .perform(
            post("{map}/move", map)
                .contentType(MediaType.APPLICATION_JSON).content(json))
        .andExpect(status().isCreated());

    verify(moveService, times(1))
        .handleMove(argThat(arg ->
            moveDto.boardId.equals(arg.boardId) &&
                moveDto.from.equals(arg.from) &&
                moveDto.to.equals(arg.to)
        ));
  }
}
