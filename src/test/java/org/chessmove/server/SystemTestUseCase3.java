package org.chessmove.server;


import com.google.gson.Gson;
import org.chessmove.server.model.dto.MoveDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SystemTestUseCase3 extends SeleniumTest {

  @LocalServerPort
  int port;

  private int boardId;

  private List<MoveDto> sampleMoves1;
  private String[] sampleFromStrings1 =
      {"a2", "a7", "b2", "b7", "c2", "c7", "d2"};
  private String[] sampleToStrings1 =
      {"a4", "a5", "b3", "b6", "c4", "c5", "d3"};

  private List<MoveDto> sampleMoves2;
  private String[] sampleFromStrings2 =
      {"d7", "e2", "e7", "f2", "f7", "g2", "g7", "h2", "h7"};
  private String[] sampleToStrings2 =
      {"d6", "e4", "e5", "f3", "f6", "g4", "g5", "h3", "h6"};

  @Before
  public void before() throws IOException, InterruptedException {
    baseUriString = baseUriStringPrefix + port;

    gson = new Gson();

    // Construct some ID-less move mock data
    sampleMoves1 = assembleMoveDTOs(sampleFromStrings1, sampleToStrings2);

    sampleMoves2 = assembleMoveDTOs(sampleFromStrings2, sampleToStrings2);

    createGames();
  }

  /**
   * <b>Preconditions:</b>
   * At least one game is being played on the system.
   */
  @Test
  public void test() throws IOException, InterruptedException {
    boardId = 5;


    // The user is on the dashboard, where active games are displayed.
    goToDashboard();

    assertTrue(checkDashboardContainsBoardId(boardId));


    // The user selects the game he wishes to spectate
    dashboardClickBoard(boardId);

    assertEquals(boardId, dashboardCurrentBoardId());

    assertEquals(sampleMoves1.size(), dashboardCurrentBoardMoveCount());


    // The game being played is displayed to the user in near real time (less than 150ms delay).
    long result = measureMoveAppearance(150);
    logger.info("It took {} ms for the move to appear in the board view.", result);
    assertTrue(result < 150);
  }

  private long measureMoveAppearance(long limit) throws IOException, InterruptedException {
    int expectedMoveCount = sampleMoves1.size() + 1;

    apiMakeMove(boardId, sampleMoves2.get(0));

    long waitStart = System.currentTimeMillis();
    long currentTime;

    do {
      if ((currentTime = System.currentTimeMillis()) > limit)
        break;
    } while (dashboardCurrentBoardMoveCount() < expectedMoveCount);

    return currentTime - waitStart;
  }

  private void createGames() throws IOException, InterruptedException {
    for (int i = 1; i < 9; i++) {
      apiStartGame(i);

      apiMakeMoves(i, sampleMoves1);
    }
  }
}
