import {
  FETCH_GAMES,
  FETCH_GAME,
  FETCH_PAST_GAMES,
  FETCH_PAST_GAME,
  FETCH_WIN_PROB
} from "./types";

const url = "http://" + window.location.host;
// const url = "http://localhost:24377";

export const fetchGames = () => async dispatch => {
  fetch(`${url}/xhr/games`)
    .then(res => res.json())
    .then(games =>
      dispatch({
        type: FETCH_GAMES,
        payload: games
      })
    );
};

export const fetchGame = game_id => async dispatch => {
  fetch(`${url}/xhr/game/${game_id}`)
    .then(res => res.json())
    .then(game =>
      dispatch({
        type: FETCH_GAME,
        payload: game
      })
    );
};

export const fetchPastGames = () => async dispatch => {
  fetch(`${url}/xhr/history`)
    .then(res => res.json())
    .then(pastGames =>
      dispatch({
        type: FETCH_PAST_GAMES,
        payload: pastGames
      })
    );
};

export const fetchPastGame = game_id => async dispatch => {
  fetch(`${url}/xhr/history`)
    .then(res => res.json())
    .then(pastGames =>
      dispatch({
        type: FETCH_PAST_GAME,
        payload: pastGames.filter(game => game.id + "" === game_id)[0]
      })
    );
};

export const endGame = (game_id, result) => async dispatch => {
  fetch(`${url}/xhr/game/${game_id}/end/${result}`, {
    method: "POST"
  });
};

export const revertGame = (game_id, move_idx) => async dispatch => {
  fetch(`${url}/xhr/game/${game_id}/revert/${move_idx}`, {
    method: "POST"
  });
};

export const gameNames = (
  game_id,
  white_name,
  black_name
) => async dispatch => {
  fetch(
    `${url}/xhr/game/${game_id}/names?black=${black_name}&white=${white_name}`,
    {
      method: "POST"
    }
  );
};

export const fetchWinProb = game_fen => async dispatch => {
  fetch(`${url}/xhr/game/prediction/?fen=${game_fen}`)
    .then(res => res.json())
    .then(winProb => {
      dispatch({
        type: FETCH_WIN_PROB,
        payload: winProb
      });
    });
};
