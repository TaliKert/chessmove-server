export const FETCH_GAMES = "FETCH_GAMES";
export const FETCH_GAME = "FETCH_GAME";
export const FETCH_PAST_GAMES = "FETCH_PAST_GAMES";
export const FETCH_PAST_GAME = "FETCH_PAST_GAME";
export const FETCH_WIN_PROB = "FETCH_WIN_PROB";
// export const END_GAME = "END_GAME";
// export const REVERT_GAME = "REVERT_GAME";
