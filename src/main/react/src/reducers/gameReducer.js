import {
  FETCH_GAMES,
  FETCH_GAME,
  FETCH_PAST_GAMES,
  FETCH_PAST_GAME,
  FETCH_WIN_PROB
  // END_GAME,
  // REVERT_GAME
} from "../actions/types";

const initialState = {
  pastItems: [],
  pastItem: {},
  items: [],
  item: {},
  winProb: undefined
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_GAMES:
      return {
        ...state,
        items: action.payload,
        item: {} // resets item when user on dashboard
      };
    case FETCH_GAME:
      return {
        ...state,
        item: action.payload
      };
    case FETCH_PAST_GAMES:
      return {
        ...state,
        pastItems: action.payload,
        pastItem: {}
      };
    case FETCH_PAST_GAME:
      return {
        ...state,
        pastItem: action.payload
      };
    case FETCH_WIN_PROB:
      return {
        ...state,
        winProb: action.payload
      };
    // case END_GAME:
    //   return {
    //     // ...state,
    //     // pastItems: action.payload
    //   };
    // case REVERT_GAME:
    //   return {
    //     // ...state,
    //     // pastItems: action.payload
    //   };
    default:
      return state;
  }
}
