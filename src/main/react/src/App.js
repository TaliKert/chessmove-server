import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from "react-redux";

import Navbar from "./components/Navbar";
import Dashboard from "./components/Dashboard";
import History from "./components/History";
import GamePage from "./components/Game/GamePage";
import GamePagePast from "./components/Game/GamePagePast";

import store from "./store.js";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Navbar></Navbar>
          <Route exact path="/game/:game_id" component={GamePage}></Route>
          <Route
            exact
            path="/history/:game_id"
            component={GamePagePast}
          ></Route>
          <Route exact path="/history" component={History}></Route>
          <Route exact path="/" component={Dashboard}></Route>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
