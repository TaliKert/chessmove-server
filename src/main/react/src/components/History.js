import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "react-router-dom";

import { fetchPastGames } from "../actions/gameActions";

class History extends Component {
  componentDidMount() {
    this.props.fetchPastGames();
  }

  leadingZero(number) {
    return (number < 10 ? "0" : "") + number;
  }

  render() {
    const renderedGames = this.props.pastGames.map((game, rowIndex) => {
      const {
        id,
        boardId,
        moveCount,
        result,
        startTime,
        endTime,
        whiteName,
        blackName
      } = game;
      const startDate = new Date(startTime);

      // Adding a link to every cell in a row
      const infoCells = [
        [id, ""],
        [startDate.toLocaleDateString("est-EE"), ""],
        [
          startDate.toLocaleString("est-EE", {
            hour: "2-digit",
            minute: "2-digit"
          }),
          "hide"
        ],
        [boardId, "hide"],
        [moveCount, "hide"],
        [
          new Date(endTime - startTime - 10800000).toLocaleString("est-EE", {
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit"
          }),
          "hide"
        ],
        [whiteName, "hide"],
        [blackName, "hide"],
        [result, ""]
      ].map((info, columnIndex) => {
        return (
          <td
            id={"table-row-" + rowIndex + "-column-" + columnIndex} // for testing
            key={id + "_" + columnIndex}
            className={info[1]}
          >
            <Link to={`/history/${id}`}>{info[0]}</Link>
          </td>
        );
      });

      return (
        <tr id={"table-row-" + rowIndex} key={id}>
          {infoCells}
        </tr>
      );
    });

    return (
      <div className="container">
        <h1>History</h1>
        <Table>
          <tbody id="table-body">
            <tr>
              <td>Game Id</td>
              <td>Date</td>
              <td className="hide">Start time</td>
              <td className="hide">Board Id</td>
              <td className="hide">Moves</td>
              <td className="hide">Duration</td>
              <td className="hide">White player</td>
              <td className="hide">Black player</td>
              <td>Result</td>
            </tr>
            {renderedGames.length === 0 ? (
              <tr>
                <td
                  style={{
                    pointerEvents: "none",
                    display: "block",
                    padding: "10px 16px",
                    marginRight: "-200px"
                  }}
                >
                  No games in history
                </td>
              </tr>
            ) : (
              renderedGames
            )}
          </tbody>
        </Table>
      </div>
    );
  }
}

const Table = styled.table`
  border: solid 1px #fff;
  border-radius: 8px;
  border-collapse: collapse;
  overflow: hidden;
  background: #fff;
  box-shadow: rgba(0, 7, 58, 0.06) 0px 5px 20px;
  width: 100%;
  font-weight: 500;
  font-size: 14px;

  a {
    display: block;
    padding: 10px 12px;
  }

  td {
    a {
      max-width: 152px;
      overflow: hidden !important;
      text-overflow: ellipsis;
    }
  }

  tr {
    :nth-child(odd) {
      background-color: #f9f9fb;
    }

    :first-child {
      pointer-events: none;
      background-color: #b58863;
      color: #fff;
      td {
        padding: 10px 12px;
      }
    }

    :hover {
      cursor: pointer;
      color: #b58863;
    }
  }

  @media (max-width: 880px) {
    .hide {
      display: none;
    }
  }
`;

History.propTypes = {
  fetchPastGames: PropTypes.func.isRequired,
  pastGames: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  pastGames: state.games.pastItems
});

export default connect(mapStateToProps, { fetchPastGames })(History);
