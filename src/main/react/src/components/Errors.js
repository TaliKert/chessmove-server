import React, { Component } from "react";
import styled from "styled-components";

class Errors extends Component {
  render() {
    const illegalStates = this.props.games
      .filter(game => game.illegalStateIndex !== -1)
      .map(game => [game.id, game.illegalStateIndex]);

    var invalidString = "";
    illegalStates.forEach(illegalState => {
      invalidString += `[game: ${illegalState[0]}, move: ${illegalState[1]}], `;
    });
    invalidString = invalidString.substring(0, invalidString.length - 1);

    return (
      <ErrorBox
        style={{
          marginBottom: "22px",
          display: illegalStates.length !== 0 ? "inherit" : "none"
        }}
      >
        <strong>Illegal moves:</strong>{" "}
        {invalidString.substring(0, invalidString.length - 1)}
      </ErrorBox>
    );
  }
}

const ErrorBox = styled.div`
  border: solid 1px #f13a47;
  border-radius: 8px;
  background: #ffe8ea;
  padding: 8px 12px;
  color: #f13a47;
  transition: all 0.2s ease-in;
`;

export default Errors;
