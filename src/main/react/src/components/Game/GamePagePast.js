import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import {
  ButtonBox,
  BoardBlock,
  DescriptionLine,
  Side,
  LinkButton
} from "../../Styled";
import { fetchPastGame, fetchWinProb } from "../../actions/gameActions";
import GameBoard from "./GameBoard";
import GameNavigation from "./GameNavigation";
import GamePlayers from "./GamePlayers";

class GamePagePast extends Component {
  state = {
    currentMove: undefined
  };

  leadingZero(number) {
    return (number < 10 ? "0" : "") + number;
  }

  componentDidMount() {
    const { game_id } = this.props.match.params;
    this.props.fetchPastGame(game_id);
  }

  componentDidUpdate() {
    const { currentMove } = this.state;
    const { moveCount, prevPositions } = this.props.pastGame;
    if (currentMove === undefined) {
      this.props.fetchWinProb(encodeURI(prevPositions[moveCount]));
      this.setState({ currentMove: moveCount });
    }
  }

  changeMove(direction) {
    const { currentMove } = this.state;
    const { prevPositions } = this.props.pastGame;
    const newMove = currentMove + direction;
    this.props.fetchWinProb(encodeURI(prevPositions[newMove]));
    this.setState({ currentMove: newMove });
  }

  render() {
    if (this.props.pastGame === undefined) this.props.history.push("/");
    const { pastGame, winProb } = this.props;
    const {
      id,
      boardId,
      moveCount,
      startTime,
      endTime,
      prevPositions,
      position,
      result,
      whiteName,
      blackName,
      illegalStateIndex
    } = pastGame;
    const { currentMove } = this.state;
    const startDate = new Date(startTime);
    const endDate = new Date(endTime);

    return (
      <div className="container">
        <BoardBlock
          illegal={illegalStateIndex !== -1 && illegalStateIndex <= currentMove}
        >
          <GameBoard
            key={pastGame.id}
            pastGame={pastGame}
            position={prevPositions ? prevPositions[currentMove] : position}
            singleBoard={true}
          ></GameBoard>
          <Side>
            <DescriptionLine>
              <span className="h2">Game id:</span>
              <span style={{ fontWeight: "700", fontSize: "19px" }}>{id}</span>
            </DescriptionLine>
            <hr />
            <DescriptionLine>
              <span className="greyTxt">duration:</span>
              <span>
                {new Date(endTime - startTime).toLocaleString("est-EE", {
                  minute: "2-digit",
                  second: "2-digit"
                })}
              </span>
            </DescriptionLine>
            <DescriptionLine>
              <span className="greyTxt">moves:</span>
              <span> {moveCount} </span>
            </DescriptionLine>
            <DescriptionLine>
              <span className="greyTxt">result:</span>
              <span> {result} </span>
            </DescriptionLine>
            <hr />
            <DescriptionLine>
              <span className="greyTxt">date:</span>
              <span>{startDate.toLocaleDateString("est-EE")}</span>
            </DescriptionLine>
            <DescriptionLine>
              <span className="greyTxt">start time:</span>
              <span>
                {startDate.toLocaleString("est-EE", {
                  hour: "2-digit",
                  minute: "2-digit"
                })}
              </span>
            </DescriptionLine>
            <DescriptionLine>
              <span className="greyTxt">end time:</span>
              <span>
                {endDate.toLocaleString("est-EE", {
                  hour: "2-digit",
                  minute: "2-digit"
                })}
              </span>
            </DescriptionLine>
            <DescriptionLine>
              <span className="greyTxt">board id:</span>
              <span>{boardId}</span>
            </DescriptionLine>
            <hr />
            <GamePlayers
              hasWinProb={true}
              blackWin={winProb}
              blackName={blackName}
              whiteName={whiteName}
              gameId={id}
            />
            <hr />
            <ButtonBox>
              <DescriptionLine>
                <span className="greyTxt">centipawn advantage:</span>
                <span className="greyTxt">{winProb}</span>
              </DescriptionLine>
              <GameNavigation
                onClick={this.changeMove.bind(this)}
                leftDisabled={currentMove === 0}
                rightDisabled={currentMove === pastGame.moveCount}
                moveNr={this.leadingZero(currentMove)}
                moveCount={this.leadingZero(moveCount)}
              />
              <LinkButton
                href={`http://${window.location.host}/xhr/game/${id}/export`}
              >
                export to PGN
              </LinkButton>
            </ButtonBox>
          </Side>
        </BoardBlock>
      </div>
    );
  }
}

GamePagePast.propTypes = {
  fetchPastGame: PropTypes.func.isRequired,
  pastGame: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  pastGame: state.games.pastItem,
  winProb: state.games.winProb
});

export default connect(mapStateToProps, { fetchPastGame, fetchWinProb })(
  GamePagePast
);
