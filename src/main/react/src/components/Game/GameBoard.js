import React, { Component } from "react";
import Chessboard from "chessboardjsx";
import styled from "styled-components";

class GameBoard extends Component {
  constructor(props) {
    super(props);
    this.state = { width: 0, height: 0 };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  calcBoardWidth(width, singleBoard) {
    let containerWidth = width * 0.8;
    if (width < 580) return width * 0.88;
    if (containerWidth > 920) containerWidth = 920;

    if (singleBoard)
      if (width < 1200) return containerWidth;
      else return containerWidth * 0.72;
    else if (width < 840) return containerWidth / 2 - 28 / 2;
    else return containerWidth / 3 - (28 * 2) / 3;
  }

  render() {
    const { boardId, singleBoard, position } = this.props;
    const { width } = this.state;
    const boardWidth = this.calcBoardWidth(width, singleBoard);

    return (
      <div>
        <Chessboard
          id={boardId}
          position={position}
          draggable={false}
          boardStyle={{}}
          showNotation={false}
          width={boardWidth}
          undo={true}
        ></Chessboard>
        <Curtain width={boardWidth + "px"}></Curtain>
      </div>
    );
  }
}

const Curtain = styled.div`
  position: relative;
  width: ${props => props.width};
  height: ${props => props.width};
  bottom: ${props => props.width};
  margin-bottom: -${props => props.width};
  z-index: 10;
`;

export default GameBoard;
