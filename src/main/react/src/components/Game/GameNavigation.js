import React, { Component } from "react";
import { Button } from "../../Styled";
import styled from "styled-components";

class GameNavigation extends Component {
  render() {
    return (
      <NavButtons>
        <Button
          id="move-back"
          disabled={this.props.leftDisabled}
          onClick={this.props.onClick.bind(this, -1)}
          style={{ transform: "scaleX(-1)" }}
        >
          {arrow}
        </Button>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            userSelect: "none"
          }}
        >
          <span className="greyTxt">move: </span>
          <span style={{ marginLeft: "8px" }}>{this.props.moveNr}</span>
          <span className="greyTxt">/{this.props.moveCount}</span>
        </div>
        <Button
          id="move-forward"
          disabled={this.props.rightDisabled}
          onClick={this.props.onClick.bind(this, 1)}
        >
          {arrow}
        </Button>
      </NavButtons>
    );
  }
}

const arrow = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 51 36"
    style={{ width: "13px" }}
  >
    <path
      d="M49 15L32 1c-3-2-7 0-7 3v10H4c-2 0-4 2-4 4s2 4 4
      4h21v10c0 3 4 5 7 3l17-13c3-2 3-6 0-7z"
    />
  </svg>
);

const NavButtons = styled.div`
  margin: 12px 0;
  width: 100%;
  display: grid;
  grid-template-columns: 34px auto 34px;
  gap: 14px;
`;

export default GameNavigation;
