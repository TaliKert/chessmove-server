import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";

import {
  ButtonBox,
  BoardBlock,
  DescriptionLine,
  Side,
  Button
} from "../../Styled";
import {
  fetchGame,
  fetchGames,
  fetchWinProb,
  endGame,
  revertGame
} from "../../actions/gameActions";
import GameBoard from "./GameBoard";
import GameNavigation from "./GameNavigation";
import GamePlayers from "./GamePlayers";

class GamePage extends Component {
  state = {
    currentMove: undefined,
    autoPlay: true
  };

  leadingZero(number) {
    return (number < 10 ? "0" : "") + number;
  }

  componentDidMount() {
    const { game_id } = this.props.match.params;
    this.props.fetchGame(game_id);
    this.interval = setInterval(() => this.props.fetchGame(game_id), 100);
  }

  componentDidUpdate() {
    const { currentMove, autoPlay } = this.state;
    const { moveCount, prevPositions, endTime } = this.props.game;

    if (currentMove === undefined && moveCount !== undefined) {
      this.props.fetchWinProb(encodeURI(prevPositions[moveCount]));
      this.setState({ currentMove: moveCount });
    }

    if (autoPlay && currentMove + 1 === moveCount) {
      this.props.fetchWinProb(encodeURI(prevPositions[moveCount]));
      this.setState({ currentMove: moveCount });
    }

    if (endTime) this.props.history.push("/history/" + this.props.game.id);
    // this.props.history.push("/history/" + this.props.game.id);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  changeMove(direction) {
    this.setState({
      autoPlay: this.state.currentMove + 1 === this.props.game.moveCount,
      currentMove: this.state.currentMove + direction
    });

    const { currentMove } = this.state;
    const { prevPositions } = this.props.game;
    const newMove = currentMove + direction;
    this.props.fetchWinProb(encodeURI(prevPositions[newMove]));
    this.setState({ currentMove: newMove });
  }

  end(winner) {
    this.props.endGame(this.props.game.id, winner);
    this.props.history.push("/");
  }

  revert() {
    this.props.revertGame(this.props.game.id, this.state.currentMove);
  }

  render() {
    const { game, winProb } = this.props;
    const {
      id,
      boardId,
      moveCount,
      startTime,
      prevPositions,
      position,
      whiteName,
      blackName,
      illegalStateIndex
    } = game;
    const { currentMove } = this.state;
    const startDate = new Date(startTime);

    return (
      <div className="container">
        <BoardBlock
          illegal={illegalStateIndex !== -1 && illegalStateIndex <= currentMove}
        >
          <GameBoard
            key={id}
            game={game}
            position={prevPositions ? prevPositions[currentMove] : position}
            singleBoard={true}
          ></GameBoard>
          <Side>
            <DescriptionLine>
              <span className="h2">Game id:</span>
              <span style={{ fontWeight: "700", fontSize: "19px" }}>{id}</span>
            </DescriptionLine>
            <hr />
            <DescriptionLine>
              <span className="greyTxt">duration:</span>
              <span>
                {new Date(Date.now() - startTime - 10800000).toLocaleString(
                  "est-EE",
                  {
                    hour: "2-digit",
                    minute: "2-digit",
                    second: "2-digit"
                  }
                )}
                {/* {new Date(Date.now() - startDate).getHours() +
                  ":" +
                  new Date(Date.now() - startDate).getMinutes()} */}
              </span>
            </DescriptionLine>
            <DescriptionLine>
              <span className="greyTxt">moves:</span>
              <span> {moveCount} </span>
            </DescriptionLine>

            <hr />
            <DescriptionLine>
              <span className="greyTxt">date:</span>
              <span>{startDate.toLocaleDateString("est-EE")}</span>
            </DescriptionLine>
            <DescriptionLine>
              <span className="greyTxt">start time:</span>
              <span>
                {startDate.toLocaleString("est-EE", {
                  hour: "2-digit",
                  minute: "2-digit"
                })}
              </span>
            </DescriptionLine>
            <DescriptionLine>
              <span className="greyTxt">board id:</span>
              <span>{boardId}</span>
            </DescriptionLine>
            <hr />
            <GamePlayers
              blackWin={0.5}
              blackName={blackName}
              whiteName={whiteName}
              gameId={id}
            />
            <hr />
            <ButtonBox>
              <div className="greyTxt" style={{ marginBottom: "12px" }}>
                end game:
              </div>
              <EndButtons>
                <Button id="end-white" onClick={this.end.bind(this, "white")}>
                  white
                </Button>
                <Button id="end-draw" onClick={this.end.bind(this, "draw")}>
                  draw
                </Button>
                <Button id="end-black" onClick={this.end.bind(this, "black")}>
                  black
                </Button>
              </EndButtons>
              <hr />
              <DescriptionLine>
                <span className="greyTxt">centipawn advantage:</span>
                <span className="greyTxt">{winProb}</span>
              </DescriptionLine>
              <GameNavigation
                onClick={this.changeMove.bind(this)}
                leftDisabled={currentMove === 0}
                rightDisabled={currentMove === game.moveCount}
                moveNr={this.leadingZero(currentMove)}
                moveCount={this.leadingZero(moveCount)}
              />
              <Button
                id="move-revert"
                disabled={currentMove === game.moveCount}
                onClick={this.revert.bind(this)}
              >
                revert to current move
              </Button>
            </ButtonBox>
          </Side>
        </BoardBlock>
      </div>
    );
  }
}

export const EndButtons = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  gap: 12px;
`;

GamePage.propTypes = {
  fetchGame: PropTypes.func.isRequired,
  endGame: PropTypes.func.isRequired,
  revertGame: PropTypes.func.isRequired,
  game: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  game: state.games.item,
  games: state.games.items,
  winProb: state.games.winProb
});

export default connect(mapStateToProps, {
  fetchGames,
  fetchGame,
  fetchWinProb,
  endGame,
  revertGame
})(GamePage);
