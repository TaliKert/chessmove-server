import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import GameBoard from "./GameBoard";

class GamePreview extends Component {
  leadingZero(number) {
    return (number < 10 ? "0" : "") + number;
  }

  render() {
    const { game } = this.props;
    const {
      id,
      position,
      boardId,
      moveCount,
      startTime,
      illegalStateIndex
    } = game;
    const moveString = this.leadingZero(moveCount);
    const timeString = new Date(
      Date.now() - startTime - 10800000
    ).toLocaleString("est-EE", {
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit"
    });
    // console.log();

    return (
      <BoardBlock illegal={illegalStateIndex !== -1}>
        <GameBoard
          key={id}
          boardId={boardId}
          position={position}
          singleBoard={false}
        ></GameBoard>
        <Description>
          <span style={{ fontWeight: "700" }}>board: {boardId}</span>
          <span
            id={"moves-" + boardId}
            className="greyTxt"
            style={{ textAlign: "center" }}
          >
            move: {moveString}
          </span>
          <span className="greyTxt" style={{ textAlign: "right" }}>
            {timeString}
          </span>
        </Description>
      </BoardBlock>
    );
  }
}

const Description = styled.p`
  font-size: 15px;
  display: grid;
  margin: 5px 10px;
  grid-template-columns: auto auto auto;
`;

const BoardBlock = styled.div`
  border: solid 1px ${props => (props.illegal ? "#f13a47" : "#fff")};
  border-radius: 8px;
  background: ${props => (props.illegal ? "#ffe8ea" : "#fff")};
  box-shadow: rgba(0, 7, 58, 0.04) 0px 5px 20px;
  overflow: hidden;
  transition: all 0.2s ease-in;
  color: ${props => (props.illegal ? "#f13a47" : "#000")};

  &:hover {
    border: solid 1px ${props => (props.illegal ? "#da0010" : "#b58863")};
    background: ${props => (props.illegal ? "#ffdadd" : "#fff")};
  }

  .greyTxt {
    ${props => (props.illegal ? "color: #f13a47" : "")};
  }
`;

GamePreview.propTypes = {
  game: PropTypes.object.isRequired
};

export default GamePreview;
