import React, { Component } from "react";
import styled from "styled-components";
import { connect } from "react-redux";

import { Button } from "../../Styled";
import { gameNames } from "../../actions/gameActions";

class GamePlayers extends Component {
  state = {
    blackName: undefined,
    whiteName: undefined
  };

  handleChangeBlack(event) {
    this.setState({ blackName: event.target.value });
  }
  handleChangeWhite(event) {
    this.setState({ whiteName: event.target.value });
  }
  handleSubmit() {
    this.props.gameNames(
      this.props.gameId,
      encodeURI(this.state.whiteName),
      encodeURI(this.state.blackName)
    );
  }

  componentDidUpdate() {
    if (
      this.state.blackName === undefined ||
      this.state.whiteName === undefined
    ) {
      this.setState({
        blackName: this.props.blackName,
        whiteName: this.props.whiteName
      });
    }
  }

  render() {
    // console.log("winProb: ", this.props.blackWin);
    // console.log(this.props.blackWin === undefined);
    return (
      <div>
        <span className="greyTxt">players:</span>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <Field>
            <input
              type="text"
              name="black-name"
              placeholder="black player"
              value={this.state.blackName || ""}
              onChange={this.handleChangeBlack.bind(this)}
            ></input>
            <PlayerColor color="#000" />
          </Field>
          <Field>
            <input
              type="text"
              name="white-name"
              placeholder="white player"
              value={this.state.whiteName || ""}
              onChange={this.handleChangeWhite.bind(this)}
            ></input>
            <PlayerColor color="#fff" />
          </Field>
        </form>
        <Button id="game-names" onClick={this.handleSubmit.bind(this)}>
          submit player names
        </Button>
      </div>
    );
  }
}

// const WinProbability = styled.div`
//   height: 8px;
//   /* border: 1px solid #000; */
//   border: 1px solid ${props => (props.disabled ? "#e1e2eb" : "#000")};
//   border-radius: 3.6px;
//   margin-top: 10px;
//   box-sizing: border-box;

//   div {
//     visibility: ${props => (props.disabled ? "hidden" : "inherit")};
//     width: ${props => props.probability}%;
//     background-color: #000;
//     margin-top: -0.1px;
//     height: calc(100% + 0.2px);
//   }
// `;

const PlayerColor = styled.div`
  display: inline-block;
  background: ${props => props.color};
  height: 100%;
  width: 8px;
  border: 1px solid #000;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  box-sizing: border-box;
`;

const Field = styled.div`
  height: 30px;
  width: 100%;
  margin: 10px 0;

  :focus-within {
    div {
      border-color: #b58863;
    }
  }

  input {
    padding: 5px 8px;
    font-family: inherit;
    font-size: 14.6px;
    font-weight: 600;
    vertical-align: top;
    width: calc(100% - 8px);
    height: 100%;

    border: 1px solid #e1e2eb;
    border-right: none;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
    box-sizing: border-box;

    :focus {
      outline: none;
      /* border-color: #000; */
      border-color: #b58863;
    }

    ::placeholder {
      color: #757682;
    }
  }
`;

export default connect(null, { gameNames })(GamePlayers);
