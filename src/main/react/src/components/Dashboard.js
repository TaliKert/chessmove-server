import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";

import { fetchGames } from "../actions/gameActions";
import GamePreview from "./Game/GamePreview";
import Errors from "./Errors";

class Dashboard extends Component {
  componentDidMount() {
    this.props.fetchGames();
    this.interval = setInterval(() => this.props.fetchGames(), 100);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const renderedGames = this.props.games.map(game => (
      <Link key={game.id} to={`/game/${game.id}`}>
        <GamePreview key={game.id} game={game}></GamePreview>
      </Link>
    ));

    return (
      <div className="container">
        <Errors games={this.props.games}></Errors>
        <h1>
          {renderedGames.length > 0 ? "Current games" : "Looking for games..."}
        </h1>
        <ChessGrid>{renderedGames}</ChessGrid>
      </div>
    );
  }
}

const ChessGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  gap: 28px;

  @media (max-width: 840px) {
    grid-template-columns: 1fr 1fr;
  }

  @media (max-width: 580px) {
    grid-template-columns: 1fr;
    gap: 6vw;
  }
`;

Dashboard.propTypes = {
  fetchGames: PropTypes.func.isRequired,
  games: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  games: state.games.items
});

export default connect(mapStateToProps, { fetchGames })(Dashboard);
