import React from "react";
import { NavLink } from "react-router-dom";
import logoVert from "../logo-vert-noicon.svg";
import styled from "styled-components";

function Navbar() {
  return (
    <Nav>
      <div className="container navbar">
        <NavLink exact to="/">
          <img src={logoVert} alt="logo" style={{ height: "1em" }} />
        </NavLink>{" "}
        <NavLink
          className="greyTxtHov"
          activeClassName="blackTxt"
          style={{ float: "right", marginLeft: "16px" }}
          to="/history"
        >
          history
        </NavLink>
        <NavLink
          className="greyTxtHov"
          exact
          activeClassName="blackTxt"
          style={{ float: "right" }}
          to="/"
        >
          dashboard
        </NavLink>
      </div>
    </Nav>
  );
}

const Nav = styled.nav`
  box-shadow: rgba(0, 7, 58, 0.04) 0px 4px 20px;
  background: #fff;
  margin-bottom: 40px;

  a {
    padding: 10px 0;
    display: inline-block;
  }
`;

export default Navbar;
