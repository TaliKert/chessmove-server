import styled from "styled-components";

export const ButtonBox = styled.div`
  bottom: 20px;
  position: absolute;
  width: calc(100% - 40px);

  @media (max-width: 1200px) {
    position: initial;
    width: 100%;
  }
`;

export const Button = styled.button`
  font-family: "Muli";
  background-color: #b58863;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 34px;
  color: #fff;
  fill: #fff;
  font-weight: 600;
  font-size: 15px;
  transition: all 0.1s ease-in;
  user-select: none;
  width: 100%;
  border: none;

  &:hover {
    background-color: #a37652;
    cursor: pointer;
  }
  &:active {
    background-color: #916643;
  }
  &:disabled {
    opacity: 0.3;
    pointer-events: none;
  }
  &:focus {
    outline: none;
  }
`;

export const LinkButton = styled.a`
  font-family: "Muli";
  background-color: #b58863;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 34px;
  color: #fff;
  fill: #fff;
  font-weight: 600;
  font-size: 15px;
  transition: all 0.1s ease-in;
  user-select: none;
  width: 100%;
  border: none;

  &:hover {
    background-color: #a37652;
    cursor: pointer;
  }
  &:active {
    background-color: #916643;
  }
  &:disabled {
    opacity: 0.3;
    pointer-events: none;
  }
  &:focus {
    outline: none;
  }
`;

export const Side = styled.div`
  padding: 20px;
  position: relative;
`;

export const DescriptionLine = styled.div`
  margin-bottom: 4px;
  span:last-child {
    text-align: right;
    float: right;
    font-weight: 600;
    font-size: 15px;
  }
`;

export const BoardBlock = styled.div`
  border: solid 1px ${props => (props.illegal ? "#f13a47" : "#fff")};
  border-radius: 8px;
  background: ${props => (props.illegal ? "#fffefe" : "#fff")};
  box-shadow: rgba(0, 7, 58, 0.06) 0px 5px 20px;
  overflow: hidden;
  display: grid;
  grid-template-columns: 72% 28%;
  color: ${props => (props.illegal ? "#f13a47" : "#000")};

  @media (max-width: 1200px) {
    grid-template-columns: 1fr;
  }

  /* input {
    background: ${props => (props.illegal ? "#fffefe" : "#fff")};
  } */
`;

// export {
//   ButtonBox,
//   EndButtons,
//   BoardBlock,
//   DescriptionLine,
//   Side,
//   Button,
//
// };
