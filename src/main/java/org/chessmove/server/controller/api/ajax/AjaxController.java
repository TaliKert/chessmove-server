package org.chessmove.server.controller.api.ajax;

import com.github.bhlangonijr.chesslib.Board;
import org.chessmove.server.controller.api.exception.GameNotFoundException;
import org.chessmove.server.engine.Stockfish;
import org.chessmove.server.model.Game;
import org.chessmove.server.model.dto.GameDto;
import org.chessmove.server.repository.GameRepository;
import org.chessmove.server.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xhr")
@CrossOrigin("*")
public class AjaxController {

  @Autowired
  GameService gameService;

  @Autowired
  GameRepository gameRepository;

  @Autowired
  Stockfish stockfishEngine;

  @GetMapping(value = "/games")
  @ResponseStatus(HttpStatus.OK)
  public List<GameDto> games() {
    return gameToGameDto(gameService.inProgressGames());
  }

  @GetMapping(value = "/game/{gameId}")
  @ResponseStatus(HttpStatus.OK)
  public GameDto games(@PathVariable Integer gameId) {
    return gameService.gameToGameDto(requireGame(gameId));
  }

  @GetMapping(value = "/history")
  @ResponseStatus(HttpStatus.OK)
  public List<GameDto> history() {
    return gameToGameDto(gameService.pastGames());
  }

  @PostMapping(value = "/game/{gameId}/revert/{moveIdx}")
  @ResponseStatus(HttpStatus.OK)
  public void revert(@PathVariable Integer gameId, @PathVariable Integer moveIdx) {
    gameService.revertMove(requireGame(gameId), moveIdx);

  }

  @GetMapping(value = "/game/{gameId}/export", produces = "text/plain")
  @ResponseStatus(HttpStatus.OK)
  public void revert(@PathVariable Integer gameId,
                     HttpServletResponse response) {
    try {
      InputStream is = new ByteArrayInputStream(gameService.gameToPGN(requireGame(gameId)).getBytes());
      org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
      response.setContentType("text/plain");
      response.setHeader("Content-Disposition", "attachment; filename=\"game" + gameId + ".pgn\"");
      response.flushBuffer();
    } catch (IOException ex) {
      throw new RuntimeException("IOError writing file to output stream");
    }
  }

  @PostMapping(value = "/game/{gameId}/end/{result}")
  @ResponseStatus(HttpStatus.OK)
  public void setWinner(@PathVariable Integer gameId, @PathVariable String result) {
    gameService.endGameInProgress(requireGame(gameId), result);
  }

  @PostMapping(value = "/game/{gameId}/names")
  @ResponseStatus(HttpStatus.OK)
  @Transactional
  public void setPlayerNames(@PathVariable Integer gameId, @PathParam("black") String black, @PathParam("white") String white) {
    Game game = requireGame(gameId);
    game.setBlackName(black);
    game.setWhiteName(white);
  }

  @GetMapping(value = "/game/prediction/")
  @ResponseStatus(HttpStatus.OK)
  public float getFenScore(@PathParam("fen") String fen) throws IOException, InterruptedException {
    try { // Validate syntax of fen string
      new Board().loadFromFen(fen);
    } catch (Exception e) {
      return 0.0f;
    }
    return stockfishEngine.getCentiPawnFromFen(fen, 2000);
  }

  private List<GameDto> gameToGameDto(List<Game> games) {
    return games
        .stream()
        .map(game -> gameService.gameToGameDto(game))
        .collect(Collectors.toList());
  }

  private Game requireGame(Integer gameId) {
    Optional<Game> gameOptional = gameRepository.findById(gameId);

    if (gameOptional.isPresent())
      return gameOptional.get();

    throw new GameNotFoundException(String.format("Game with id %d not found.", gameId));
  }
}
