package org.chessmove.server.controller.api.board;

import org.chessmove.server.model.dto.MoveDto;
import org.chessmove.server.service.GameService;
import org.chessmove.server.service.MoveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class BoardController {

  @Autowired
  MoveService moveService;

  @Autowired
  GameService gameService;

  @PostMapping(value = "/start/{boardId}")
  @ResponseStatus(HttpStatus.CREATED)
  public void start(@PathVariable String boardId) {
    gameService.initiateGame(boardId);
  }

  @PostMapping(value = "/move", consumes = "application/json")
  @ResponseStatus(HttpStatus.CREATED)
  public void move(@RequestBody MoveDto moveDTO) {
    moveService.handleMove(moveDTO);
  }

}
