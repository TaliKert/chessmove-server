package org.chessmove.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexRedirectController {

  @RequestMapping("/**/{path:[^\\.]+}")
  public String redir() {
    return "forward:/";
  }

}
