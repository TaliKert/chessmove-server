package org.chessmove.server.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Inspiration from
 * https://github.com/rahular/chess-misc GPL-2.0
 */
@Component
public class Stockfish {

  Logger logger = LoggerFactory.getLogger(Stockfish.class);

  @Value("${chessmove.engine.path}")
  private String PATH;

  private Process initEngine() {
    if (!Files.exists(Path.of(PATH))) {
      logger.info("Chess engine initiation failed. Not found in classpath");
    } else {
      try {
        return Runtime.getRuntime().exec(PATH);
      } catch (IOException e) {
        logger.warn("Chess engine initiation failed with exception", e);
      }
    }
    return null;
  }

  public float getCentiPawnFromFen(String fen, long moveTime) throws IOException, InterruptedException {

    Process proc = initEngine();
    if (proc == null) return 0.0f;

    try (BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
         BufferedWriter procWriter = new BufferedWriter(new OutputStreamWriter(proc.getOutputStream()))) {

      procWriter.write(String.format("position fen %s\n", fen));
      procWriter.write(String.format("go movetime %d\n", moveTime));
      procWriter.flush();

      Thread.sleep(moveTime + 20);
      procWriter.write("quit\n");

      Pattern pattern = Pattern.compile("score cp (-?\\d*)");

      String line;
      float result = 0.0f;
      while (procReader.ready() && (line = procReader.readLine()) != null) {
        Matcher m = pattern.matcher(line);
        if (m.find() && !StringUtils.isEmpty(m.group(1)))
          result = Float.parseFloat(m.group(1));
      }
      return result / 100;

    } finally {
      proc.destroy();
    }
  }
}