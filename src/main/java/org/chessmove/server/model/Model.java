package org.chessmove.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public class Model {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  private boolean deleted = false;

}
