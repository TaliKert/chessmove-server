package org.chessmove.server.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MoveDto {
  public String boardId;
  public String to;
  public String from;
}
