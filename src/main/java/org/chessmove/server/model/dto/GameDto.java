package org.chessmove.server.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.chessmove.server.model.Game;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GameDto {
  public Integer id;          // Game identifier
  public String boardId;      // Board identifier
  public Long startTime;      // Start timestamp in epoch time milliseconds
  public Long endTime;      // end timestamp in epoch time milliseconds
  public Integer moveCount;   // amount of moves made
  public String position;     // Current position of the game in FEN notation
  public List<String> prevPositions; // Chronologically ordered list of moves in FEN notation, excluding the last position
  public String result;
  public Integer illegalStateIndex = -1;
  public String whiteName; // name of white player
  public String blackName; // name of black player

  public GameDto(Game game) {
    this.id = game.getId();
    this.boardId = game.getBoardId();
    this.startTime = game.getStartTime().toEpochMilli();
    this.endTime = game.getEndTime() == null ? null : game.getEndTime().toEpochMilli();
    this.result = game.getResult();
    this.whiteName = game.getWhiteName();
    this.blackName = game.getBlackName();
  }
}
