package org.chessmove.server.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.Instant;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Game extends Model {

  @Column(name = "board_id")
  private String boardId;

  @Column(name = "name_white")
  private String whiteName;

  @Column(name = "name_black")
  private String blackName;

  @Column(name = "start_time")
  private Instant startTime;

  @Column(name = "end_time")
  private Instant endTime;

  @Column(name = "result")
  private String result;

  @OneToMany(mappedBy = "game")
  @Where(clause = "deleted = 0")
  private List<Move> moves;

  public Game(String boardId) {
    this.boardId = boardId;
    this.startTime = Instant.now();
  }
}
