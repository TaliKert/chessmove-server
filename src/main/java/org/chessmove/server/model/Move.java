package org.chessmove.server.model;

import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.Square;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.chessmove.server.model.dto.MoveDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Move extends Model {

  @ManyToOne
  private Game game;

  @Column(name = "moved_from")
  private String from;

  @Column(name = "moved_to")
  private String to;

  private Instant time;

  public Move(MoveDto moveDTO) {
    this.game = null;
    this.from = moveDTO.getFrom();
    this.to = moveDTO.getTo();
    this.time = Instant.now();
  }

  public Square getFromSquare() {
    return Square.valueOf(this.from.toUpperCase());
  }

  public Square getToSquare() {
    return Square.valueOf(this.to.toUpperCase());
  }

  public com.github.bhlangonijr.chesslib.move.Move getLibMove() {
    return new com.github.bhlangonijr.chesslib.move.Move(getFromSquare(), getToSquare());
  }

  public com.github.bhlangonijr.chesslib.move.Move getLibMove(Piece piece) {
    return new com.github.bhlangonijr.chesslib.move.Move(getFromSquare(), getToSquare(), piece);
  }


  public boolean isStationary() {
    return this.getFromSquare() == this.getToSquare();
  }
}
