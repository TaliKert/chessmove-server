package org.chessmove.server.helper;

import com.github.bhlangonijr.chesslib.*;
import com.github.bhlangonijr.chesslib.game.GameContext;
import com.github.bhlangonijr.chesslib.move.Move;
import org.chessmove.server.model.Game;
import org.chessmove.server.service.MoveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CastlingHelper {

  @Autowired
  MoveService moveService;

  private static final GameContext GC = new GameContext();

  public boolean isPotentialCastlingMove(Move libMove) {
    return GC.isCastleMove(libMove) || isRookCastleMove(libMove);
  }

  private boolean isRookCastleMove(Move libMove) {
    for (Side side : new Side[]{Side.WHITE, Side.BLACK}) {
      for (CastleRight castleRight : new CastleRight[]{CastleRight.KING_SIDE, CastleRight.QUEEN_SIDE}) {
        if (libMove.equals(GC.getRookCastleMove(side, castleRight)))
          return true;
      }
    }
    return false;
  }


  /**
   * A forgiving handler for castling
   * The cases that this method takes into account are as follows:
   * 1. Current move is king and the rook has not moved   -> do nothing
   * 2. Current move is rook and the king has not moved   -> do nothing
   * 3. Current move is king but the rook has moved first -> soft delete previous rook move
   * 4. Current move is rook but the king has moved first -> soft delete current move
   */
  public void handleCastlingForgiving(org.chessmove.server.model.Move move, Game game) {
    Move libMove = move.getLibMove();
    Board board = moveService.getBoard(game.getMoves());


    if (board.getBackup().isEmpty()) return;
    MoveBackup lastMove = board.getBackup().getLast(); // Preserve data of previous move
    board.undoMove();
    board.doMove(libMove); // Do this move as the last move didn't happen
    if (board.getBackup().isEmpty()) return;
    MoveBackup currentMove = board.getBackup().getLast();

    if (currentMove.getMovingPiece().getPieceSide() == lastMove.getMovingPiece().getPieceSide()) {

      Side sideInQuestion = libMove.getTo().getRank().compareTo(Rank.RANK_4) > 0 ?
          Side.BLACK : Side.WHITE;
      CastleRight castleRightInQuestion = libMove.getTo().getFile().compareTo(File.FILE_D) > 0 ?
          CastleRight.KING_SIDE : CastleRight.QUEEN_SIDE;

      // Case No.3
      if (lastMove.getMovingPiece().getPieceType() == PieceType.ROOK &&
          lastMove.getMove().equals(GC.getRookCastleMove(sideInQuestion, castleRightInQuestion)) &&
          currentMove.getMovingPiece().getPieceType() == PieceType.KING &&
          currentMove.getMove().equals(GC.getKingCastleMove(sideInQuestion, castleRightInQuestion))) {
        game.getMoves().get(game.getMoves().size() - 1).setDeleted(true);
      }

      // Case No.4
      if (lastMove.getMovingPiece().getPieceType() == PieceType.KING &&
          lastMove.getMove().equals(GC.getKingCastleMove(sideInQuestion, castleRightInQuestion)) &&
          currentMove.getMovingPiece().getPieceType() == PieceType.ROOK &&
          currentMove.getMove().equals(GC.getRookCastleMove(sideInQuestion, castleRightInQuestion))) {
        move.setDeleted(true);
      }
    }
  }
}
