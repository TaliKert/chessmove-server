package org.chessmove.server.service;

import com.github.bhlangonijr.chesslib.*;
import com.github.bhlangonijr.chesslib.move.MoveGenerator;
import com.github.bhlangonijr.chesslib.move.MoveGeneratorException;
import org.chessmove.server.controller.api.exception.GameNotStartedException;
import org.chessmove.server.helper.CastlingHelper;
import org.chessmove.server.model.Game;
import org.chessmove.server.model.Move;
import org.chessmove.server.model.dto.MoveDto;
import org.chessmove.server.repository.MoveRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Service
public class MoveService {

  @Autowired
  GameService gameService;

  @Autowired
  MoveRepository moveRepository;

  @Autowired
  CastlingHelper castlingHelper;

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @Transactional
  public void handleMove(MoveDto moveDTO) {
    Game game = gameService.findGameInProgress(moveDTO.getBoardId());

    if (game == null)
      throw new GameNotStartedException(
          String.format("Game not started on board with id %s", moveDTO.getBoardId()));

    Move move = new Move(moveDTO);

    if (hasPieceOnToSquare(move, game)) {
      if (move.isStationary()) {
        move.setDeleted(true);
        checkGameEnd(game, move);
      }

      if (castlingHelper.isPotentialCastlingMove(move.getLibMove())) {
        castlingHelper.handleCastlingForgiving(move, game);
      }
    } else
      move.setDeleted(true);

    move.setGame(game);
    moveRepository.save(move);
  }

  private boolean hasPieceOnToSquare(Move move, Game game) {
    if (getBoard(game.getMoves()).getPiece(Square.fromValue(move.getFrom().toUpperCase())) != Piece.NONE)
      return true;
    logger.warn("A move was sent with a from position with no piece: {}", move.getFrom());
    return false;
  }


  public void checkGameEnd(Game game, Move invoker) {
    List<Move> moves = moveRepository.findAllByGameOrderByTimeDesc(game);

    Iterator<Move> moveIterator = moves.iterator();
    if (!moveIterator.hasNext()) return;

    Move lastMove = moveIterator.next();
    if (!lastMove.isStationary()) return;

    if (moveIterator.hasNext() && invoker.getLibMove().equals(lastMove.getLibMove())) {
      // Surrender case, check second-to-last move
      Move secondToLastMove = moveIterator.next();

      if (!invoker.getLibMove().equals(secondToLastMove.getLibMove())) return;

      Board board = getBoard(game.getMoves());

      for (Side side : new Side[]{Side.WHITE, Side.BLACK}) {
        if (invoker.getFromSquare() == board.getKingSquare(side))
          gameService.endGameInProgress(game, side.flip().value().toLowerCase());
      }

    } else {
      // Draw case
      Board board = getBoard(game.getMoves());

      for (Side side : new Side[]{Side.WHITE, Side.BLACK}) {
        if (invoker.getFromSquare() == board.getKingSquare(side) &&
            lastMove.getFromSquare() == board.getKingSquare(side.flip()))
          gameService.endGameInProgress(game, "draw");
      }
    }
  }

  public Board getBoard(List<Move> moves) {
    Board board = new Board();

    for (Move move : moves) {
      board.doMove(promotionCheck(move, board));
    }

    return board;
  }

  private com.github.bhlangonijr.chesslib.move.Move promotionCheck(Move move, Board board) {
    com.github.bhlangonijr.chesslib.move.Move libMove;
    if (board.getPiece(move.getFromSquare()) == Piece.WHITE_PAWN && (move.getToSquare().getRank() == Rank.RANK_1 || move.getToSquare().getRank() == Rank.RANK_8))
      libMove = move.getLibMove(Piece.WHITE_QUEEN);
    else if (board.getPiece(move.getFromSquare()) == Piece.BLACK_PAWN && (move.getToSquare().getRank() == Rank.RANK_1 || move.getToSquare().getRank() == Rank.RANK_8))
      libMove = move.getLibMove(Piece.BLACK_QUEEN);
    else
      libMove = move.getLibMove();
    return libMove;
  }

  @Transactional
  public LinkedList<String> getFenList(Game game) {
    List<Move> moves = moveRepository.findAllByGameAndDeletedIsFalseOrderByTimeAsc(game);

    return getFenList(moves);
  }

  public LinkedList<String> getFenList(List<Move> moves) {
    Board board = new Board();

    LinkedList<String> fens = new LinkedList<>();
    fens.add(board.getFen());

    boolean isIllegalStateReached = false;
    int illegalStateIndex = 0;

    for (Move move : moves) {
      com.github.bhlangonijr.chesslib.move.Move libMove = promotionCheck(move, board);

      if (!isIllegalStateReached) {
        illegalStateIndex++;

        isIllegalStateReached = isMoveIllegal(libMove, board);
      }

      board.doMove(libMove);

      fens.add(board.getFen());
    }

    fens.addFirst(isIllegalStateReached ? "" + illegalStateIndex : null);

    return fens;
  }

  private boolean isMoveIllegal(com.github.bhlangonijr.chesslib.move.Move libMove, Board board) {
    try {
      return !board.isMoveLegal(libMove, true) ||
          !MoveGenerator.generateLegalMoves(board).contains(libMove);
    } catch (MoveGeneratorException e) {
      return true;
    }
  }
}
