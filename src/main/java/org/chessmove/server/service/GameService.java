package org.chessmove.server.service;

import com.github.bhlangonijr.chesslib.game.GameResult;
import com.github.bhlangonijr.chesslib.move.MoveConversionException;
import com.github.bhlangonijr.chesslib.move.MoveList;
import org.chessmove.server.controller.api.exception.GameNotParsableException;
import org.chessmove.server.model.Game;
import org.chessmove.server.model.Move;
import org.chessmove.server.model.dto.GameDto;
import org.chessmove.server.repository.GameRepository;
import org.chessmove.server.repository.MoveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

@Service
public class GameService {

  @Autowired
  GameRepository gameRepository;

  @Autowired
  MoveService moveService;

  @Autowired
  MoveRepository moveRepository;

  public Game findGameInProgress(String boardId) {
    return gameRepository.findGameByBoardIdEqualsAndEndTimeIsNull(boardId);
  }

  public void initiateGame(String boardId) {
    Game oldGame = findGameInProgress(boardId);
    if (oldGame != null)
      endGameInProgress(oldGame, null);

    Game newGame = new Game(boardId);
    gameRepository.save(newGame);
  }

  @Transactional
  public void endGameInProgress(Game game, String result) {
    if (game.getEndTime() == null) {
      game.setResult(result);
      game.setEndTime(Instant.now());
    }
  }

  public List<Game> inProgressGames() {
    return gameRepository.findGamesByEndTimeIsNull();
  }

  public GameDto gameToGameDto(Game game) {
    GameDto gameDto = new GameDto(game);
    LinkedList<String> fens = moveService.getFenList(game);

    // First element says if we found an illegal move:
    //  null - no illegal move
    //  numeric string - illegal state index
    String first = fens.pop();
    if (first != null)
      gameDto.illegalStateIndex = Integer.parseInt(first);

    gameDto.setPosition(fens.getLast());
    gameDto.setPrevPositions(fens);
    gameDto.setMoveCount(moveRepository.countMovesByGameAndDeletedIsFalse(game));

    return gameDto;
  }

  @Transactional
  public void revertMove(Game game, Integer moveIdx) {
    // Found the returned List to be ArrayList specifically, indexing is fast.
    List<Move> moves = moveRepository.findAllByGameAndDeletedIsFalseOrderByTimeAsc(game);

    for (int i = moveIdx; i < moves.size(); i++)
      moves.get(i).setDeleted(true);
  }

  public List<Game> pastGames() {
    return gameRepository.findGamesByEndTimeIsNotNullOrderByIdDesc();
  }

  // Export game in PGN file format
  public String gameToPGN(Game game) {
    com.github.bhlangonijr.chesslib.game.Event event = new com.github.bhlangonijr.chesslib.game.Event();
    event.setName("-");
    event.setSite("-");
    event.setStartDate(game.getStartTime().toString());

    com.github.bhlangonijr.chesslib.game.Round round = new com.github.bhlangonijr.chesslib.game.Round(event);
    round.setNumber(0);

    com.github.bhlangonijr.chesslib.game.Game gameObject = new com.github.bhlangonijr.chesslib.game.Game(game.getId().toString(), round);

    com.github.bhlangonijr.chesslib.game.Player whitePlayer = new com.github.bhlangonijr.chesslib.game.GenericPlayer();
    whitePlayer.setName(game.getWhiteName());
    whitePlayer.setElo(0);
    gameObject.setWhitePlayer(whitePlayer);

    com.github.bhlangonijr.chesslib.game.Player blackPlayer = new com.github.bhlangonijr.chesslib.game.GenericPlayer();
    blackPlayer.setName(game.getBlackName());
    blackPlayer.setElo(0);
    gameObject.setBlackPlayer(blackPlayer);

    com.github.bhlangonijr.chesslib.game.GameResult result;
    switch (game.getResult()) {
      case "white":
        result = GameResult.WHITE_WON;
        break;
      case "black":
        result = GameResult.BLACK_WON;
        break;
      default:
        result = GameResult.DRAW;
    }

    gameObject.setResult(result);

    gameObject.setPlyCount(moveRepository.countMovesByGameAndDeletedIsFalse(game).toString());

    gameObject.setBoard(moveService.getBoard(game.getMoves()));

    MoveList moveList = new MoveList();
    for (Move move : moveRepository.findAllByGameAndDeletedIsFalseOrderByTimeAsc(game)) {
      moveList.add(move.getLibMove());
    }

    gameObject.setHalfMoves(moveList);
    gameObject.setMoveText(new StringBuilder());


    try {
      return gameObject.toPgn(false, false);
    } catch (MoveConversionException e) {
      throw new GameNotParsableException();
    }
  }
}
