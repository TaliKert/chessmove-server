package org.chessmove.server.repository;

import org.chessmove.server.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, Integer> {

  Game findGameByBoardIdEqualsAndEndTimeIsNull(String boardId);

  List<Game> findGamesByEndTimeIsNull();

  List<Game> findGamesByEndTimeIsNotNullOrderByIdDesc();
}
