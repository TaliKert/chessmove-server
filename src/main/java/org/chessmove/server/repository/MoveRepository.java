package org.chessmove.server.repository;

import org.chessmove.server.model.Game;
import org.chessmove.server.model.Move;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MoveRepository extends JpaRepository<Move, Integer> {
  List<Move> findAllByGameAndDeletedIsFalseOrderByTimeAsc(Game game);

  List<Move> findAllByGameOrderByTimeDesc(Game game);

  Integer countMovesByGameAndDeletedIsFalse(Game game);
}
