# kasutamine: python testboard.py 127.0.0.1:8080 inputFile1.txt inputFile2.txt ...

import json
import os
import re
import sys
from random import randint


# a class to hold all the moves in a game
class Game:
    def __init__(self, gameID: int, filePath: str = None):
        self.gameId: int = gameID

        self.started: bool = False
        self.moves: [(str, str)] = []
        self.moveId: int = 0
        self.moveCount: int = 0

        # also gonna parse the moves file here
        if filePath is not None:
            self.parseFile(filePath)

    def parseFile(self, filePath: str) -> None:
        """parses a file, calls parseLine() for each line"""
        with open(filePath, "r", encoding="UTF-8") as f:
            lines: [str] = f.readlines()

            for line in lines:
                self.parseLine(line)

            self.moveCount = len(self.moves)

    def parseLine(self, line: str) -> None:
        """parses each line,
        if it doesn't meet the requirements, is ignored, but is mentioned in CLI"""
        parts: [str] = re.split("\s+", re.sub("{.*?\}", "", line.strip()))

        if len(parts) != 2 or None in [re.match("^[a-h][1-8]$", part) for part in parts]:
            print("Game id {}: Move id {} not parsed: {}".format(self.gameId,
                                                                 self.moveCount,
                                                                 parts))
            return

        self.moves.append(tuple(parts))
        self.moveCount += 1

    def getMove(self) -> {str: str} or None:
        """returns either the current move or None"""
        if self.moveId >= self.moveCount:
            return None

        fromTo: (str, str) = self.moves[self.moveId]

        return {"boardId": str(self.gameId),
                "from": fromTo[0],
                "to": fromTo[1]}

    def next(self) -> None:
        """moves to the next move"""
        self.moveId += 1

    def restart(self):
        """goes back to start with the moves"""
        self.moveId = 0


def getDefaultMoves():
    game: Game = Game(123)

    for move in [("a2", "a4"), ("a7", "a6"), ("b1", "b3"), ("b7", "b5"), ("a4", "b5")]:
        game.moves.append(move)

    game.moveCount = len(game.moves)

    return game


# not using it anymore
def defaultMovesOld(ip: str):
    command: str = "curl -X POST http://{}/api/start/{}".format(ip, 123)

    while True:
        response: int = os.system(command)

        if response == 0:
            print("> Game id {} started.".format(123))
            break

        input("Failed to start the game, press ENTER key to retry!")

    moves = [
        {"boardId": "123", "from": "a2", "to": "a4"},
        {"boardId": "123", "from": "a7", "to": "a6"},
        {"boardId": "123", "from": "b1", "to": "b3"},
        {"boardId": "123", "from": "b7", "to": "b5"},
        {"boardId": "123", "from": "a4", "to": "b5"}
    ]

    ip = sys.argv[1]
    site = sys.argv[2]

    for item in moves:
        input("Press any key to send move...")

        response = os.system("curl -d '" + json.dumps(item).replace(" ", "") +
                             "'" + ' -H "Content-Type: application/json" '
                                   '-X POST' + " http://" + ip + site)

        if response == 0:
            print("Sent!")
        else:
            print("Failed!")

        print()
        print()

    print("DONE")


def startGame(game: Game, ip: str):
    """Deals with starting the game given"""

    game.started = True

    command: str = "curl -X POST http://{}/api/start/{}".format(ip, game.gameId)

    while True:
        response: int = os.system(command)

        if response == 0:
            print("> Game id {} started.".format(game.gameId))
            break

        input("Failed to start the game, press any key to retry!")


def sendMove(game: Game, ip: str) -> None:
    """Deals with sending out the move given"""

    move: (str, str) = game.getMove()

    command: str = ("curl -d {} -H \"Content-Type: application/json\" -X POST http://{}/api/move/"
                    .format(json.dumps(move).replace(" ", "").replace("\"", "\\\""), ip))

    while True:
        response: int = os.system(command)

        if response == 0:
            print("> {}: {}".format(game.gameId, move))
            game.next()
            return

        input("Failed to start the game, press any key to retry!")


if __name__ == "__main__":
    ip: str = sys.argv[1]

    idCounter: int = 0

    argn: int = len(sys.argv)

    games: [Game] = []
    gamesFinished: [Game] = []

    if argn < 3:
        games.append(getDefaultMoves())

    for i in range(2, argn):
        games.append(Game(idCounter, sys.argv[i]))
        idCounter += 1

    print("\nGames have been parsed\n")

    n: int = len(games)

    while n > 0:
        input("Press any key to send move...")

        pick: int = randint(0, n - 1)

        game: Game = games[pick]

        if not game.started:
            startGame(game, ip)
            continue

        if game.getMove() is None:
            gamesFinished.append(games.pop(pick))
            n -= 1
            print("Game id {} ended.".format(game.gameId))
            continue

        sendMove(game, ip)

    print("DONE!")
